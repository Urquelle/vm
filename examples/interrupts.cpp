#include <windows.h>
#include <assert.h>

#undef  ALLOCATOR
#define ALLOCATOR(name) void *name(size_t size)
typedef ALLOCATOR(Alloc);

#undef  DEALLOCATOR
#define DEALLOCATOR(name) void name(void *mem)
typedef DEALLOCATOR(Dealloc);

#include "os.cpp"
#include "vm.cpp"
#include "mapper_device.cpp"
#include "mem_device.cpp"
#include "bank_device.cpp"

#include "lex.cpp"
#include "parser.cpp"
#include "bytecode.cpp"

ALLOCATOR(alloc_default) {
    void *result = malloc(size);

    return result;
}

DEALLOCATOR(dealloc_default) {
    free(mem);
}

Alloc   * alloc   = alloc_default;
Dealloc * dealloc = dealloc_default;
#define allocs(name) (name *)alloc(sizeof(name))

int main(int argc, char* argv[]) {
    using namespace Urq::api;

    Mapper_Device *mapper = new Mapper_Device();
    Cpu *cpu = cpu_create(mapper);

    size_t bank_size = 0xff;
    size_t num_banks = 8;
    Bank_Device *bank = new Bank_Device(num_banks, bank_size, cpu);
    mapper->map(bank, 0, bank_size);

    uint8_t data[0xff01] = {};
    Mem_Device *mem = new Mem_Device(data, 0xff01);
    mapper->map(mem, bank_size, 0xffff, true);

    mapper->write16(0x1000 + 0x00, 0x2000);
    mapper->write16(0x1000 + 0x02, 0x3000);

    /* interrupt 1 */ {
        uint8_t prog[] = {
            OP_MOV_IMM_REG, 0x42, 0x00, REG_R1,
            OP_MOV_IMM_REG, 0x55, 0x00, REG_R2,
            OP_ADD_REG_REG, REG_R1, REG_R2,
            OP_RTI
        };

        mapper->load(0x2000, prog, sizeof(prog));
    }

    /* interrupt 2 */ {
        uint8_t prog[] = {
            OP_MOV_IMM_REG, 0x65, 0x00, REG_R1,
            OP_MOV_IMM_REG, 0x22, 0x00, REG_R2,
            OP_XOR_REG_REG, REG_R1, REG_R2,
            OP_RTI
        };

        mapper->load(0x3000, prog, sizeof(prog));
    }

    /* hauptprogramm */ {
        uint8_t prog[] = {
            OP_MOV_IMM_REG, 0x01, 0x00, REG_R1,
            OP_MOV_IMM_REG, 0x02, 0x00, REG_R2,
            OP_MOV_IMM_REG, 0x03, 0x00, REG_R3,
            OP_MOV_IMM_REG, 0x04, 0x00, REG_R4,
            OP_PSH_IMM,     0x05, 0x00,
            OP_INT,         0x00, 0x00,
            OP_POP, REG_R1,
            OP_PSH_IMM,     0x06, 0x00,
            OP_PSH_IMM,     0x07, 0x00,
        };

        mapper->load(0x0000, prog, sizeof(prog));
    }

    for ( ;; ) {
        getchar();
        auto ret = step(cpu);

        debug_print(cpu);
        mem_print(cpu->mem, cpu->pc, "PC");
        mem_print(cpu->mem, 0xffff - 42 - 1, "STACK", 43);
        mem_print(cpu->mem, 0xffff - 31, "MEMORY", 16);
        mem_print(cpu->mem, 0xffff - 15, "MEMORY", 16);

        if ( ret == OP_HLT ) {
            printf("\n=============== PROGRAMM ENDE =================\n");
            getchar();
            break;
        }
    }

    return 0;
}


