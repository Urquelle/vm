#include <windows.h>
#include <assert.h>

#undef  ALLOCATOR
#define ALLOCATOR(name) void *name(size_t size)
typedef ALLOCATOR(Alloc);

#undef  DEALLOCATOR
#define DEALLOCATOR(name) void name(void *mem)
typedef DEALLOCATOR(Dealloc);

#include "os/os.cpp"
#include "vm/vm.cpp"
#include "asm/asm.cpp"

ALLOCATOR(alloc_default) {
    void *result = malloc(size);

    return result;
}

DEALLOCATOR(dealloc_default) {
    free(mem);
}

Alloc   * alloc   = alloc_default;
Dealloc * dealloc = dealloc_default;
#define allocs(name) (name *)alloc(sizeof(name))

int main(int argc, char* argv[]) {
    using namespace Urq::Os::api;
    using namespace Urq::Vm::api;
    using namespace Urq::Asm::api;

    char *content = "";
    os_file_read(argv[1], &content);

    auto tokens = tokenize(argv[1], content);
    auto ast    = parse(&tokens);
    auto prog   = bytecode(&ast);

    Mem_Device *mem = new Mem_Device(prog->code, prog->size);
    Cpu *cpu = cpu_create(mem);

    debug_print(cpu);
    mem_print(cpu->mem, cpu->pc, "PC");
    mem_print(cpu->mem, 0xffff - 42 - 1, "STACK", 43);
    mem_print(cpu->mem, 0x0050, "MEMORY", 10);

    for ( ;; ) {
        getchar();

        auto ret = step(cpu);

        debug_print(cpu);
        mem_print(cpu->mem, cpu->pc, "PC");
        mem_print(cpu->mem, 0xffff - 42 - 1, "STACK", 43);
        mem_print(cpu->mem, 0x0050, "MEMORY", 10);

        if ( ret == OP_HLT ) {
            printf("\n=============== PROGRAMM ENDE =================\n");
            getchar();
            break;
        }
    }

    return 0;
}

