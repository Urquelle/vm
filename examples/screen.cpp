#include <windows.h>
#include <assert.h>

#include "vm/vm.cpp"

void
write_char(Urq::Vm::Cpu *cpu, uint16_t *pc, char c, uint8_t cmd, uint8_t pos) {
    using namespace Urq::Vm::api;

    *pc += cpu->mem->write(*pc, OP_MOV_IMM_REG);
    *pc += cpu->mem->write(*pc, c);
    *pc += cpu->mem->write(*pc, cmd);
    *pc += cpu->mem->write(*pc, REG_R1);

    *pc += cpu->mem->write(*pc, OP_MOV_REG_MEM);
    *pc += cpu->mem->write(*pc, REG_R1);

    *pc += cpu->mem->write(*pc, pos);
    *pc += cpu->mem->write(*pc, 0x30);
}

int main(int argc, char const* argv[]) {
    using namespace Urq::Vm::api;

    size_t mem_size = 256*256;
    uint8_t *mem = mem_create(mem_size);

    Mem_Device *mem_device = new Mem_Device(mem, mem_size);
    Mapper_Device *mm = new Mapper_Device();

    mm->map(mem_device, 0, 0xffff);
    mm->map(new Screen_Device(), 0x3000, 0x30ff, MAPPER_REGION_REMAP);

    Cpu *cpu = cpu_create(mm);

    /* ============ PROGRAMM =========== */
    uint16_t pc = 0;

    write_char(cpu, &pc, ' ', 0xff, 0);
    for ( uint8_t i = 0; i < 0xff; ++i ) {

        uint8_t cmd = ( i % 2 ) == 0
            ? 0x01
            : 0x02;

        write_char(cpu, &pc, '*', cmd, i);
    }

    pc += cpu->mem->write(pc, OP_HLT);
    /* ================================= */

    run(cpu);

    return 0;
}

