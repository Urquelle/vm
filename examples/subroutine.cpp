#include <assert.h>

#include "vm/vm.cpp"

int main(int argc, char const* argv[]) {
    using namespace Urq::Vm::api;

    size_t mem_size = 256*256;
    uint8_t *mem = mem_create(mem_size);

    Mem_Device *mem_device = new Mem_Device(mem, mem_size);
    Mapper_Device *mm = new Mapper_Device();

    mm->map(mem_device, 0, 0xffff);
    mm->map(new Screen_Device(), 0x3000, 0x30ff, MAPPER_REGION_REMAP);

    Cpu *cpu = cpu_create(mm);

    /* ============ PROGRAMM =========== */
    uint16_t pc = 0;
    uint16_t subroutine_address = 0x3000;

    pc += cpu->mem->write(pc, OP_PSH_IMM);
    pc += cpu->mem->write16(pc, 0x3333);

    pc += cpu->mem->write(pc, OP_PSH_IMM);
    pc += cpu->mem->write16(pc, 0x2222);

    pc += cpu->mem->write(pc, OP_PSH_IMM);
    pc += cpu->mem->write16(pc, 0x1111);

    pc += cpu->mem->write(pc, OP_MOV_IMM_REG);
    pc += cpu->mem->write16(pc, 0x1234);
    pc += cpu->mem->write(pc, REG_R1);

    pc += cpu->mem->write(pc, OP_MOV_IMM_REG);
    pc += cpu->mem->write16(pc, 0x5678);
    pc += cpu->mem->write(pc, REG_R4);

    pc += cpu->mem->write(pc, OP_PSH_IMM);
    pc += cpu->mem->write16(pc, 0x0000);

    pc += cpu->mem->write(pc, OP_CAL_IMM);
    pc += cpu->mem->write16(pc, subroutine_address);

    pc += cpu->mem->write(pc, OP_PSH_IMM);
    pc += cpu->mem->write16(pc, 0x4444);

    pc = subroutine_address;

    pc += cpu->mem->write(pc, OP_PSH_IMM);
    pc += cpu->mem->write16(pc, 0x0102);

    pc += cpu->mem->write(pc, OP_PSH_IMM);
    pc += cpu->mem->write16(pc, 0x0304);

    pc += cpu->mem->write(pc, OP_PSH_IMM);
    pc += cpu->mem->write16(pc, 0x0506);

    pc += cpu->mem->write(pc, OP_MOV_IMM_REG);
    pc += cpu->mem->write16(pc, 0x0708);
    pc += cpu->mem->write(pc, REG_R1);

    pc += cpu->mem->write(pc, OP_MOV_IMM_REG);
    pc += cpu->mem->write16(pc, 0x090A);
    pc += cpu->mem->write(pc, REG_R8);

    pc += cpu->mem->write(pc, OP_RET);
    /* ================================= */

    debug_print(cpu);
    mem_print(cpu->mem, cpu->pc, "PC");
    mem_print(cpu->mem, 0xffff - 42 - 1, "STACK", 43);

    for ( ;; ) {
        getchar();

        step(cpu);

        debug_print(cpu);
        mem_print(cpu->mem, cpu->pc, "PC");
        mem_print(cpu->mem, 0xffff - 42 - 1, "STACK", 43);
    }

    return 0;
}


