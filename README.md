# Normal Level Virtual Machine

virtual machine based on [Low Level Javascript](https://github.com/LowLevelJavaScript/16-Bit-Virtual-Machine)

## functionality

- 16 bit virtual machine with stack and 8 general-purpose registers
- subroutine calls with agreed upon convention for storing and restoring the state
- memory mapped io
- memory extension with memory bank device
- interrupts
- labels
- data declaration
- struct declarations


