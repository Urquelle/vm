#ifndef __URQ_SSS__
#define __URQ_SSS__

namespace Urq { namespace Sss {

#include "lex.cpp"
#include "parser.cpp"
#include "resolver.cpp"

namespace api {
    using Urq::Sss::tokenize;
    using Urq::Sss::parse;
    using Urq::Sss::resolve;
}

}}

#endif

