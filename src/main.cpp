#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <vector>
#include <cctype>
#include <assert.h>

#include "os/os.cpp"
#include "util/util.cpp"
#include "vm/vm.cpp"
#include "asm/asm.cpp"
#include "sss/sss.cpp"

Arena *perm_arena;
Arena *temp_arena;

ALLOCATOR(custom_alloc) {
    void *result = arena_alloc(perm_arena, size);

    return result;
}

ALLOCATOR(custom_alloct) {
    void *result = arena_alloc(temp_arena, size);

    return result;
}

REALLOCATOR(custom_realloc) {
    void *result = realloc(mem, size);

    return result;
}

DEALLOCATOR(custom_dealloc) {
}

CALLOCATOR(custom_calloc) {
    void *result = calloc(num, size);

    return result;
}

int main(int argc, char* argv[]) {
    using namespace Urq::Os::api;

    perm_arena = arena_new(1024);
    temp_arena = arena_new(1024);

    urq_alloc   = custom_alloc;
    urq_alloct  = custom_alloct;
    urq_calloc  = custom_calloc;
    urq_realloc = custom_realloc;
    urq_dealloc = custom_dealloc;

#if 0
    using namespace Urq::Vm::api;
    using namespace Urq::Asm::api;

    char *content = "";
    os_file_read(argv[1], &content);

    auto tokens = tokenize(argv[1], content);
    auto ast    = parse(&tokens);
    auto prog   = bytecode(&ast);

    auto program_start = label_addr("code");
    auto hero_data  = var_addr("hero");

    Mapper_Dev *mm = mapper_dev_create();
    Cpu *cpu = cpu_create(mm);

    size_t num_banks = 8;
    size_t bank_size = 0xff;
    Bank_Dev *bank = bank_dev_create(num_banks, bank_size, cpu);
    mapper_dev_map(mm, bank, 0, bank_size);

    Mem_Dev *mem = mem_dev_create(prog->code, prog->size);
    mapper_dev_map(mm, mem, bank_size, 0xffff, MAPPER_REGION_REMAP);

    cpu->pc = (uint16_t)(bank_size + program_start);

    debug_print(cpu);
    mem_print(cpu->mem, cpu->pc, "PC");
    mem_print(cpu->mem, 0xffff - 42 - 1, "STACK", 43);
    mem_print(cpu->mem, 0, "PROGRAMM", 16);
    mem_print(cpu->mem, hero_data, "HERO", 16);
    mem_print(cpu->mem, 0x1234, "C0DE", 4);

    for ( ;; ) {
        getchar();

        auto ret = step(cpu);

        debug_print(cpu);
        mem_print(cpu->mem, cpu->pc, "PC");
        mem_print(cpu->mem, 0xffff - 42 - 1, "STACK", 43);
        mem_print(cpu->mem, 0, "PROGRAMM", 16);
        mem_print(cpu->mem, hero_data, "HERO", 16);
        mem_print(cpu->mem, 0x1234, "C0DE", 4);

        if ( ret == OP_HLT ) {
            printf("\n===================== PROGRAMM ENDE ===========================\n");
            getchar();
            break;
        }
    }
#else
    using namespace Urq::Sss::api;

    char *content = "";
    os_file_read(argv[1], &content);

    auto tokens = tokenize(argv[1], content);
    auto ast    = parse(&tokens);
#endif

    return 0;
}


