namespace Urq { namespace Vm {

struct Dev;

#include "cpu.cpp"
#include "dev.cpp"

uint8_t  mem_read(uint8_t *mem, size_t addr);
uint16_t mem_read16(uint8_t *mem, size_t addr);
uint8_t  mem_write(uint8_t *mem, size_t addr, uint8_t val);
uint8_t  mem_write16(uint8_t *mem, size_t addr, uint16_t val);

enum Op {
    OP_MOV_IMM_REG = 0x10,
    OP_MOV_IMM_MEM,
    OP_MOV_IMM_OFF_REG,
    OP_MOV_REG_REG,
    OP_MOV_REG_MEM,
    OP_MOV_REG_PTR_REG,
    OP_MOV_MEM_REG,

    OP_ADD_REG_REG,
    OP_ADD_IMM_REG,
    OP_SUB_REG_REG,
    OP_SUB_IMM_REG,
    OP_SUB_REG_IMM,
    OP_MUL_IMM_REG,
    OP_MUL_REG_REG,
    OP_INC_REG,
    OP_DEC_REG,

    OP_LSF_REG_IMM,
    OP_LSF_REG_REG,
    OP_RSF_REG_IMM,
    OP_RSF_REG_REG,
    OP_AND_REG_IMM,
    OP_AND_REG_REG,
    OP_OR_REG_IMM,
    OP_OR_REG_REG,
    OP_XOR_REG_IMM,
    OP_XOR_REG_REG,

    OP_JMP,
    OP_JNE_IMM,
    OP_JNE_REG,
    OP_JEQ_IMM,
    OP_JEQ_REG,
    OP_JLT_IMM,
    OP_JLT_REG,
    OP_JGT_IMM,
    OP_JGT_REG,
    OP_JLE_IMM,
    OP_JLE_REG,
    OP_JGE_IMM,
    OP_JGE_REG,

    OP_PSH_IMM,
    OP_PSH_REG,
    OP_CAL_IMM,
    OP_CAL_REG,
    OP_INT,

    OP_NOT,
    OP_POP,
    OP_RET,
    OP_HLT,
    OP_RTI,

    OP_COUNT,
};

uint16_t
reg_read(Cpu *cpu, uint8_t reg) {
    assert(reg < REG_COUNT);

    uint16_t result = cpu->regs[reg];

    return result;
}

void
reg_write(Cpu *cpu, uint8_t reg, uint16_t val) {
    assert(reg < REG_COUNT);

    cpu->regs[reg] = val;
}

uint8_t *
mem_create(size_t size) {
    uint8_t *result = (uint8_t *)urq_alloc(size);

    return result;
}

uint8_t
mem_read(uint8_t *mem, size_t addr) {
    uint8_t result = bit_read8(mem, addr);

    return result;
}

uint16_t
mem_read16(uint8_t *mem, size_t addr) {
    uint16_t result = bit_read16(mem, addr);

    return result;
}

uint8_t
mem_write(uint8_t *mem, size_t addr, uint8_t val) {
    uint8_t result = bit_write8(mem, addr, val);

    return result;
}

uint8_t
mem_write16(uint8_t *mem, size_t addr, uint16_t val) {
    uint8_t result = bit_write16(mem, addr, val);

    return result;
}

void
mem_print(Dev *device, size_t addr, char *label, size_t n = 8) {
    device->print(device, addr, label, n);
}

uint8_t
fetch(Cpu *cpu) {
    uint8_t instr = cpu->mem->read(cpu->mem, cpu->pc);
    cpu->pc += 1;

    return instr;
}

uint16_t
fetch16(Cpu *cpu) {
    uint16_t instr = cpu->mem->read16(cpu->mem, cpu->pc);
    cpu->pc += 2;

    return instr;
}

void
push(Cpu *cpu, uint16_t val) {
    cpu->mem->write16(cpu->mem, cpu->sp, val);
    cpu->sp -= 2;
    cpu->stack_frame_size += 2;
}

uint16_t
pop(Cpu *cpu) {
    cpu->sp += 2;
    uint16_t result = cpu->mem->read16(cpu->mem, cpu->sp);
    cpu->stack_frame_size -= 2;

    return result;
}

void
push_state(Cpu *cpu) {
    push(cpu, reg_read(cpu, REG_R1));
    push(cpu, reg_read(cpu, REG_R2));
    push(cpu, reg_read(cpu, REG_R3));
    push(cpu, reg_read(cpu, REG_R4));
    push(cpu, reg_read(cpu, REG_R5));
    push(cpu, reg_read(cpu, REG_R6));
    push(cpu, reg_read(cpu, REG_R7));
    push(cpu, reg_read(cpu, REG_R8));
    push(cpu, cpu->pc);
    push(cpu, cpu->stack_frame_size+2);

    cpu->fp = cpu->sp;
    cpu->stack_frame_size = 0;
}

void
pop_state(Cpu *cpu) {
    cpu->sp = cpu->fp;
    cpu->stack_frame_size = pop(cpu);
    auto stack_frame_size = cpu->stack_frame_size;

    cpu->pc = pop(cpu);
    reg_write(cpu, REG_R8, pop(cpu));
    reg_write(cpu, REG_R7, pop(cpu));
    reg_write(cpu, REG_R6, pop(cpu));
    reg_write(cpu, REG_R5, pop(cpu));
    reg_write(cpu, REG_R4, pop(cpu));
    reg_write(cpu, REG_R3, pop(cpu));
    reg_write(cpu, REG_R2, pop(cpu));
    reg_write(cpu, REG_R1, pop(cpu));

    auto num_args = pop(cpu);
    for ( int i = 0; i < num_args; ++i ) {
        pop(cpu);
    }

    cpu->fp = cpu->fp + stack_frame_size;
}

void
handle_interrupt(Cpu *cpu, uint16_t val) {
    uint16_t interrupt_bit = val % 0xf;

    bool masked = (((uint16_t)(1 << interrupt_bit) & cpu->im) >> interrupt_bit) == 0;
    if ( masked ) {
        return;
    }

    size_t addr_ptr = cpu->interrupt_vector_address + (interrupt_bit * 2);
    size_t addr = cpu->mem->read16(cpu->mem, addr_ptr);

    if ( !cpu->in_int ) {
        push(cpu, 0);
        push_state(cpu);
    }

    cpu->in_int = true;
    cpu->pc = (uint16_t)addr;
}

void
exec(Cpu *cpu, uint16_t instr) {
    switch ( instr ) {
        case OP_MOV_IMM_REG: {
            uint16_t imm = fetch16(cpu);
            uint8_t reg = fetch(cpu);

            reg_write(cpu, reg, imm);
        } break;

        case OP_MOV_REG_REG: {
            auto src = fetch(cpu);
            auto dst = fetch(cpu);
            uint16_t val = reg_read(cpu, src);

            reg_write(cpu, dst, val);
        } break;

        case OP_MOV_IMM_MEM: {
            uint16_t imm = fetch16(cpu);
            uint16_t addr = fetch16(cpu);

            cpu->mem->write16(cpu->mem, addr, imm);
        } break;

        case OP_MOV_REG_PTR_REG: {
            uint8_t src = fetch(cpu);
            uint8_t dst = fetch(cpu);
            uint16_t addr = reg_read(cpu, src);
            uint16_t val = cpu->mem->read16(cpu->mem, addr);
            reg_write(cpu, dst, val);
        } break;

        case OP_MOV_IMM_OFF_REG: {
            uint16_t base_addr = fetch16(cpu);
            uint8_t offset_reg = fetch(cpu);
            uint8_t dst = fetch(cpu);
            uint16_t offset = reg_read(cpu, offset_reg);
            uint16_t val = cpu->mem->read16(cpu->mem, base_addr + offset);
            reg_write(cpu, dst, val);
        } break;

        case OP_MOV_REG_MEM: {
            auto reg  = fetch(cpu);
            auto addr = fetch16(cpu);
            uint16_t val = reg_read(cpu, reg);

            cpu->mem->write16(cpu->mem, addr, val);
        } break;

        case OP_MOV_MEM_REG: {
            auto addr = fetch16(cpu);
            auto reg  = fetch(cpu);
            uint16_t val = cpu->mem->read16(cpu->mem, addr);

            reg_write(cpu, reg, val);
        } break;

        case OP_ADD_REG_REG: {
            auto r1 = fetch(cpu);
            auto r2 = fetch(cpu);
            auto imm1 = reg_read(cpu, r1);
            auto imm2 = reg_read(cpu, r2);

            reg_write(cpu, REG_ACC, imm1 + imm2);
        } break;

        case OP_ADD_IMM_REG: {
            auto imm1 = fetch16(cpu);
            auto reg = fetch(cpu);
            auto imm2 = reg_read(cpu, reg);

            reg_write(cpu, REG_ACC, imm1 + imm2);
        } break;

        case OP_SUB_REG_REG: {
            auto r1 = fetch(cpu);
            auto r2 = fetch(cpu);
            auto imm1 = reg_read(cpu, r1);
            auto imm2 = reg_read(cpu, r2);

            reg_write(cpu, REG_ACC, imm1 - imm2);
        } break;

        case OP_SUB_IMM_REG: {
            auto imm1 = fetch16(cpu);
            auto reg = fetch(cpu);
            auto imm2 = reg_read(cpu, reg);

            reg_write(cpu, REG_ACC, imm1 - imm2);
        } break;

        case OP_SUB_REG_IMM: {
            auto reg  = fetch(cpu);
            auto imm1 = reg_read(cpu, reg);
            auto imm2 = fetch16(cpu);

            reg_write(cpu, REG_ACC, imm1 - imm2);
        } break;

        case OP_INC_REG: {
            auto reg = fetch(cpu);
            auto val = reg_read(cpu, reg);

            reg_write(cpu, reg, val + 1);
        } break;

        case OP_DEC_REG: {
            auto reg = fetch(cpu);
            auto val = reg_read(cpu, reg);

            reg_write(cpu, reg, val - 1);
        } break;

        case OP_MUL_IMM_REG: {
            auto imm = fetch16(cpu);
            auto reg = fetch(cpu);
            auto val = reg_read(cpu, reg);

            reg_write(cpu, REG_ACC, imm * val);
        } break;

        case OP_MUL_REG_REG: {
            auto r1 = fetch(cpu);
            auto r2 = fetch(cpu);
            auto val1 = reg_read(cpu, r1);
            auto val2 = reg_read(cpu, r2);

            reg_write(cpu, REG_ACC, val1 * val2);
        } break;

        case OP_LSF_REG_IMM: {
            auto reg = fetch(cpu);
            auto shift = fetch16(cpu);
            auto val = reg_read(cpu, reg);

            reg_write(cpu, reg, val << shift);
        } break;

        case OP_LSF_REG_REG: {
            auto r1 = fetch(cpu);
            auto r2 = fetch(cpu);

            auto val = reg_read(cpu, r1);
            auto shift = reg_read(cpu, r2);

            reg_write(cpu, r1, val << shift);
        } break;

        case OP_RSF_REG_IMM: {
            auto reg = fetch(cpu);
            auto shift = fetch16(cpu);
            auto val = reg_read(cpu, reg);

            reg_write(cpu, reg, val >> shift);
        } break;

        case OP_RSF_REG_REG: {
            auto r1 = fetch(cpu);
            auto r2 = fetch(cpu);

            auto val = reg_read(cpu, r1);
            auto shift = reg_read(cpu, r2);

            reg_write(cpu, r1, val >> shift);
        } break;

        case OP_AND_REG_IMM: {
            auto r1 = fetch(cpu);
            auto imm = fetch16(cpu);
            auto val = reg_read(cpu, r1);

            reg_write(cpu, REG_ACC, imm & val);
        } break;

        case OP_AND_REG_REG: {
            auto r1 = fetch(cpu);
            auto r2 = fetch(cpu);

            auto val1 = reg_read(cpu, r1);
            auto val2 = reg_read(cpu, r2);

            reg_write(cpu, REG_ACC, val1 & val2);
        } break;

        case OP_OR_REG_IMM: {
            auto r1 = fetch(cpu);
            auto imm = fetch16(cpu);
            auto val = reg_read(cpu, r1);

            reg_write(cpu, REG_ACC, imm | val);
        } break;

        case OP_OR_REG_REG: {
            auto r1 = fetch(cpu);
            auto r2 = fetch(cpu);

            auto val1 = reg_read(cpu, r1);
            auto val2 = reg_read(cpu, r2);

            reg_write(cpu, REG_ACC, val1 | val2);
        } break;

        case OP_XOR_REG_IMM: {
            auto r1 = fetch(cpu);
            auto imm = fetch16(cpu);
            auto val = reg_read(cpu, r1);

            reg_write(cpu, REG_ACC, imm ^ val);
        } break;

        case OP_XOR_REG_REG: {
            auto r1 = fetch(cpu);
            auto r2 = fetch(cpu);

            auto val1 = reg_read(cpu, r1);
            auto val2 = reg_read(cpu, r2);

            reg_write(cpu, REG_ACC, val1 ^ val2);
        } break;

        case OP_NOT: {
            auto reg = fetch(cpu);
            auto val = reg_read(cpu, reg);

            reg_write(cpu, REG_ACC, (~val) & 0xffff);
        } break;

        case OP_JMP: {
            auto addr = fetch16(cpu);

            cpu->pc = addr;
        } break;

        case OP_JNE_IMM: {
            auto imm = fetch16(cpu);
            auto addr = fetch16(cpu);
            auto acc = reg_read(cpu, REG_ACC);

            if ( imm != acc ) {
                cpu->pc = addr;
            }
        } break;

        case OP_JNE_REG: {
            auto reg  = fetch(cpu);
            auto val  = reg_read(cpu, reg);
            auto addr = fetch16(cpu);

            if ( val != reg_read(cpu, REG_ACC) ) {
                cpu->pc = addr;
            }
        } break;

        case OP_JEQ_REG: {
            auto reg  = fetch(cpu);
            auto val  = reg_read(cpu, reg);
            auto addr = fetch16(cpu);

            if ( val == reg_read(cpu, REG_ACC) ) {
                cpu->pc = addr;
            }
        } break;

        case OP_JEQ_IMM: {
            auto val  = fetch16(cpu);
            auto addr = fetch16(cpu);

            if ( val == reg_read(cpu, REG_ACC) ) {
                cpu->pc = addr;
            }
        } break;

        case OP_JLT_REG: {
            auto reg  = fetch(cpu);
            auto val  = reg_read(cpu, reg);
            auto addr = fetch16(cpu);

            if ( val < reg_read(cpu, REG_ACC) ) {
                cpu->pc = addr;
            }
        } break;

        case OP_JLT_IMM: {
            auto val  = fetch16(cpu);
            auto addr = fetch16(cpu);

            if ( val < reg_read(cpu, REG_ACC) ) {
                cpu->pc = addr;
            }
        } break;

        case OP_JGT_REG: {
            auto reg  = fetch(cpu);
            auto val  = reg_read(cpu, reg);
            auto addr = fetch16(cpu);

            if ( val > reg_read(cpu, REG_ACC) ) {
                cpu->pc = addr;
            }
        } break;

        case OP_JGT_IMM: {
            auto val  = fetch16(cpu);
            auto addr = fetch16(cpu);

            if ( val > reg_read(cpu, REG_ACC) ) {
                cpu->pc = addr;
            }
        } break;

        case OP_JLE_REG: {
            auto reg  = fetch(cpu);
            auto val  = reg_read(cpu, reg);
            auto addr = fetch16(cpu);

            if ( val <= reg_read(cpu, REG_ACC) ) {
                cpu->pc = addr;
            }
        } break;

        case OP_JLE_IMM: {
            auto val  = fetch16(cpu);
            auto addr = fetch16(cpu);

            if ( val <= reg_read(cpu, REG_ACC) ) {
                cpu->pc = addr;
            }
        } break;

        case OP_JGE_REG: {
            auto reg  = fetch(cpu);
            auto val  = reg_read(cpu, reg);
            auto addr = fetch16(cpu);

            if ( val >= reg_read(cpu, REG_ACC) ) {
                cpu->pc = addr;
            }
        } break;

        case OP_JGE_IMM: {
            auto val  = fetch16(cpu);
            auto addr = fetch16(cpu);

            if ( val >= reg_read(cpu, REG_ACC) ) {
                cpu->pc = addr;
            }
        } break;

        case OP_PSH_IMM: {
            auto val = fetch16(cpu);
            push(cpu, val);
        } break;

        case OP_PSH_REG: {
            auto reg = fetch(cpu);
            auto val = reg_read(cpu, reg);
            push(cpu, val);
        } break;

        case OP_POP: {
            auto reg = fetch(cpu);
            auto val = pop(cpu);
            reg_write(cpu, reg, val);
        } break;

        case OP_CAL_IMM: {
            auto addr = fetch16(cpu);
            push_state(cpu);
            cpu->pc = addr;
        } break;

        case OP_CAL_REG: {
            auto reg = fetch(cpu);
            auto addr = reg_read(cpu, reg);
            push_state(cpu);
            cpu->pc = addr;
        } break;

        case OP_INT: {
            uint16_t val = fetch16(cpu) & 0xf;
            handle_interrupt(cpu, val);
        } break;

        case OP_RET: {
            pop_state(cpu);
        } break;

        case OP_RTI: {
            cpu->in_int = false;
            pop_state(cpu);
        } break;
    }
}

uint8_t
step(Cpu *cpu) {
    auto instr = fetch(cpu);
    exec(cpu, instr);

    return instr;
}

void
run(Cpu *cpu) {
    for ( ;; ) {
        auto ret = step(cpu);

        if ( ret == OP_HLT ) {
            break;
        }
    }
}

void
cpu_print(Cpu *cpu) {
    printf("[PC] = 0x%04x, [ACC] = 0x%04x\n", cpu->pc, cpu->regs[REG_ACC]);
    for ( int i = REG_R1; i < REG_COUNT; ++i ) {
        printf("[REG%d] = 0x%04x\n", i, cpu->regs[i]);
    }
}

void debug_print(Cpu *cpu) {
    printf("\n======================= REGS =============================\n");
    cpu_print(cpu);
}

namespace api {
    using Urq::Vm::bank_dev_create;
    using Urq::Vm::cpu_create;
    using Urq::Vm::mem_dev_create;
    using Urq::Vm::mapper_dev_create;
    using Urq::Vm::mem_create;
    using Urq::Vm::mem_write16;
    using Urq::Vm::mem_write;
    using Urq::Vm::debug_print;

    using Urq::Vm::OP_MOV_IMM_REG;
    using Urq::Vm::OP_MOV_REG_REG;
    using Urq::Vm::OP_MOV_REG_MEM;
    using Urq::Vm::OP_MOV_MEM_REG;
    using Urq::Vm::OP_MOV_IMM_MEM;
    using Urq::Vm::OP_MOV_REG_PTR_REG;
    using Urq::Vm::OP_MOV_IMM_OFF_REG;

    using Urq::Vm::OP_ADD_REG_REG;
    using Urq::Vm::OP_ADD_IMM_REG;
    using Urq::Vm::OP_SUB_REG_REG;
    using Urq::Vm::OP_SUB_IMM_REG;
    using Urq::Vm::OP_SUB_REG_IMM;
    using Urq::Vm::OP_INC_REG;
    using Urq::Vm::OP_DEC_REG;
    using Urq::Vm::OP_MUL_IMM_REG;
    using Urq::Vm::OP_MUL_REG_REG;

    using Urq::Vm::OP_LSF_REG_IMM;
    using Urq::Vm::OP_LSF_REG_REG;
    using Urq::Vm::OP_RSF_REG_IMM;
    using Urq::Vm::OP_RSF_REG_REG;
    using Urq::Vm::OP_AND_REG_IMM;
    using Urq::Vm::OP_AND_REG_REG;
    using Urq::Vm::OP_OR_REG_IMM;
    using Urq::Vm::OP_OR_REG_REG;
    using Urq::Vm::OP_XOR_REG_IMM;
    using Urq::Vm::OP_XOR_REG_REG;
    using Urq::Vm::OP_NOT;

    using Urq::Vm::OP_JNE_IMM;
    using Urq::Vm::OP_JNE_REG;
    using Urq::Vm::OP_JEQ_IMM;
    using Urq::Vm::OP_JEQ_REG;
    using Urq::Vm::OP_JLT_IMM;
    using Urq::Vm::OP_JLT_REG;
    using Urq::Vm::OP_JGT_IMM;
    using Urq::Vm::OP_JGT_REG;
    using Urq::Vm::OP_JLE_IMM;
    using Urq::Vm::OP_JLE_REG;
    using Urq::Vm::OP_JGE_IMM;
    using Urq::Vm::OP_JGE_REG;

    using Urq::Vm::OP_PSH_IMM;
    using Urq::Vm::OP_PSH_REG;
    using Urq::Vm::OP_POP;
    using Urq::Vm::OP_CAL_IMM;
    using Urq::Vm::OP_CAL_REG;
    using Urq::Vm::OP_RET;
    using Urq::Vm::OP_HLT;
    using Urq::Vm::OP_RTI;
    using Urq::Vm::OP_INT;

    using Urq::Vm::Mem_Dev;
    using Urq::Vm::Mapper_Dev;
    using Urq::Vm::Screen_Dev;
    using Urq::Vm::Bank_Dev;
    using Urq::Vm::Cpu;

    using Urq::Vm::MAPPER_REGION_REMAP;
    using Urq::Vm::MAPPER_REGION_NO_REMAP;

    using Urq::Vm::REG_R1;
    using Urq::Vm::REG_R2;
    using Urq::Vm::REG_R3;
    using Urq::Vm::REG_R4;
    using Urq::Vm::REG_R5;
    using Urq::Vm::REG_R6;
    using Urq::Vm::REG_R7;
    using Urq::Vm::REG_R8;

    using Urq::Vm::cpu_create;
}

}}

