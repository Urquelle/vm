#define DEVICE_LOAD(name)    void     name(Dev *d, size_t addr, uint8_t *data, size_t size)
#define DEVICE_READ(name)    uint8_t  name(Dev *d, size_t addr)
#define DEVICE_READ16(name)  uint16_t name(Dev *d, size_t addr)
#define DEVICE_WRITE(name)   uint8_t  name(Dev *d, size_t addr, uint8_t val)
#define DEVICE_WRITE16(name) uint8_t  name(Dev *d, size_t addr, uint16_t val)
#define DEVICE_PRINT(name)   void     name(Dev *d, size_t addr, char *label, size_t n)

struct Dev;

typedef DEVICE_LOAD(Device_Load);
typedef DEVICE_READ(Device_Read);
typedef DEVICE_READ16(Device_Read16);
typedef DEVICE_WRITE(Device_Write);
typedef DEVICE_WRITE16(Device_Write16);
typedef DEVICE_PRINT(Device_Print);

struct Dev {
    Device_Load    *load;
    Device_Read    *read;
    Device_Read16  *read16;
    Device_Write   *write;
    Device_Write16 *write16;
    Device_Print   *print;
};

struct Mem_Dev : Dev {
    uint8_t *mem;
    size_t   size;
};

enum {
    MAPPER_REGION_NO_REMAP,
    MAPPER_REGION_REMAP,
};
struct Mapper_Region {
    Dev    * device;
    size_t   start;
    size_t   end;
    uint8_t  remap;
};
struct Mapper_Dev : Dev {
    Queue regions;
    bool error;
    char *msg;
};

struct Screen_Dev : Dev {
};

struct Bank_Dev : Dev {
    Mem_Dev    ** banks;
    size_t        num_banks;
    size_t        bank_size;
    Cpu         * cpu;
};

/* mem device {{{ */
DEVICE_LOAD(mem_dev_load) {
    Mem_Dev *dev = (Mem_Dev *)d;
    bit_load(dev->mem, addr, data, size);
}

DEVICE_READ(mem_dev_read) {
    Mem_Dev *dev = (Mem_Dev *)d;

    uint8_t result = bit_read8(dev->mem, addr);

    return result;
}

DEVICE_READ16(mem_dev_read16) {
    Mem_Dev *dev = (Mem_Dev *)d;

    uint16_t result = bit_read16(dev->mem, addr);

    return result;
}

DEVICE_WRITE(mem_dev_write) {
    Mem_Dev *dev = (Mem_Dev *)d;

    uint8_t result = bit_write8(dev->mem, addr, val);

    return result;
}

DEVICE_WRITE16(mem_dev_write16) {
    Mem_Dev *dev = (Mem_Dev *)d;

    uint8_t result = bit_write16(dev->mem, addr, val);

    return result;
}

DEVICE_PRINT(mem_dev_print) {
    Mem_Dev *dev = (Mem_Dev *)d;

    printf("\n========================== %s ============================\n", label);
    printf("                         MEM 0x%04zx\n", addr);

    for ( uint16_t i = 0; i < n; ++i ) {
        if ( i % 14 == 0 && i > 0 ) {
            printf("\n");
        }

        auto val = dev->read(dev, addr+i);
        printf("0x%02x ", val);
    }
}

Mem_Dev *
mem_dev_create(uint8_t *mem, size_t size) {
    Mem_Dev *result = urq_allocs(Mem_Dev);

    result->load    = mem_dev_load;
    result->read    = mem_dev_read;
    result->read16  = mem_dev_read16;
    result->write   = mem_dev_write;
    result->write16 = mem_dev_write16;
    result->print   = mem_dev_print;

    result->mem     = mem;
    result->size    = size;

    return result;
}
/* }}} */
/* mapper dev {{{ */
Mapper_Region *
mapper_region(Dev *device, size_t start, size_t end,
        uint8_t remap = MAPPER_REGION_NO_REMAP)
{
    Mapper_Region *result = urq_allocs(Mapper_Region);

    result->device = device;
    result->start  = start;
    result->end    = end;
    result->remap  = remap;

    return result;
}

Mapper_Region *
mapper_dev_find_region(Mapper_Dev *dev, size_t addr) {
    for ( int i = 0; i < dev->regions.num_elems; ++i ) {
        Mapper_Region *region = (Mapper_Region *)queue_entry(&dev->regions, i);

        if ( region->start <= addr && addr <= region->end ) {
            return region;
        }
    }

    return NULL;
}

DEVICE_LOAD(mapper_dev_load) {
    Mapper_Dev *dev = (Mapper_Dev *)d;

    Mapper_Region *region = mapper_dev_find_region(dev, addr);

    if ( !region ) {
        dev->error = true;
        dev->msg = "addresse konnte nicht gefunden werden";
    }

    size_t actual_addr = region->remap == MAPPER_REGION_REMAP
        ? addr - region->start
        : addr;

    region->device->load(region->device, actual_addr, data, size);
}

DEVICE_READ(mapper_dev_read) {
    Mapper_Dev *dev = (Mapper_Dev *)d;

    Mapper_Region *region = mapper_dev_find_region(dev, addr);

    if ( !region ) {
        dev->error = true;
        dev->msg = "addresse konnte nicht gefunden werden";
    }

    size_t actual_addr = region->remap == MAPPER_REGION_REMAP
        ? addr - region->start
        : addr;

    return region->device->read(region->device, actual_addr);
}

DEVICE_READ16(mapper_dev_read16) {
    Mapper_Dev *dev = (Mapper_Dev *)d;

    Mapper_Region *region = mapper_dev_find_region(dev, addr);

    if ( !region ) {
        dev->error = true;
        dev->msg = "addresse konnte nicht gefunden werden";
    }

    size_t actual_addr = region->remap == MAPPER_REGION_REMAP
        ? addr - region->start
        : addr;

    return region->device->read16(region->device, actual_addr);
}

DEVICE_WRITE(mapper_dev_write) {
    Mapper_Dev *dev = (Mapper_Dev *)d;

    Mapper_Region *region = mapper_dev_find_region(dev, addr);

    if ( !region ) {
        dev->error = true;
        dev->msg = "addresse konnte nicht gefunden werden";
    }

    size_t actual_addr = region->remap == MAPPER_REGION_REMAP
        ? addr - region->start
        : addr;

    return region->device->write(region->device, actual_addr, val);
}

DEVICE_WRITE16(mapper_dev_write16) {
    Mapper_Dev *dev = (Mapper_Dev *)d;

    Mapper_Region *region = mapper_dev_find_region(dev, addr);

    if ( !region ) {
        dev->error = true;
        dev->msg = "addresse konnte nicht gefunden werden";
    }

    size_t actual_addr = region->remap == MAPPER_REGION_REMAP
        ? addr - region->start
        : addr;

    return region->device->write16(region->device, actual_addr, val);
}

DEVICE_PRINT(mapper_dev_print) {
    Mapper_Dev *dev = (Mapper_Dev *)d;

    Mapper_Region *region = mapper_dev_find_region(dev, addr);

    if ( !region ) {
        dev->error = true;
        dev->msg = "addresse konnte nicht gefunden werden";
    }

    size_t actual_addr = region->remap == MAPPER_REGION_REMAP
        ? addr - region->start
        : addr;

    return region->device->print(region->device, actual_addr, label, n);
}

auto
mapper_dev_map(Mapper_Dev *mm, Dev *dev, size_t start, size_t end,
        uint8_t remap = MAPPER_REGION_NO_REMAP)
{
    auto region = mapper_region(dev, start, end, remap);

    queue_unshift(&mm->regions, region);

    return [mm, region]() {
        queue_remove(&mm->regions, region);
    };
}

Mapper_Dev *
mapper_dev_create() {
    Mapper_Dev *result = urq_allocs(Mapper_Dev);

    result->load    = mapper_dev_load;
    result->read    = mapper_dev_read;
    result->read16  = mapper_dev_read16;
    result->write   = mapper_dev_write;
    result->write16 = mapper_dev_write16;
    result->print   = mapper_dev_print;
    result->regions = {};

    return result;
}
/* }}} */
/* screen dev {{{ */
void
screen_dev_clear_screen() {
    printf("\x1b[2J");
}

void
screen_dev_set_bold() {
    printf("\x1b[1m");
}

void
screen_dev_set_regular() {
    printf("\x1b[0m");
}

void
screen_dev_move_to(size_t x, size_t y) {
    printf("\x1b[%zd;%zdH", y, x);
}

DEVICE_LOAD(screen_dev_load) {
}

DEVICE_READ(screen_dev_read) {
    return 0;
}

DEVICE_READ16(screen_dev_read16) {
    return 0;
}

DEVICE_WRITE(screen_dev_write) {
    return 0;
}

DEVICE_WRITE16(screen_dev_write16) {
    Screen_Dev *dev = (Screen_Dev *)d;

    auto cmd = (val & 0xff00) >> 8;
    auto chr = val & 0x00ff;

    if ( cmd == 0xff ) {
        screen_dev_clear_screen();
    } else if ( cmd == 0x01 ) {
        screen_dev_set_bold();
    } else if ( cmd == 0x02 ) {
        screen_dev_set_regular();
    }

    size_t x = (addr % 16) + 1;
    size_t y = (uint16_t)(addr / 16) + 1;

    screen_dev_move_to(x*2, y);
    printf("%c", chr);

    return 0;
}

DEVICE_PRINT(screen_dev_print) {
}
/* }}} */
/* bank dev {{{ */
DEVICE_LOAD(bank_dev_load) {
    Bank_Dev *dev = (Bank_Dev *)d;
    Mem_Dev *mem = dev->banks[dev->cpu->mb];

    mem->load(mem, addr, data, size);
}

DEVICE_READ(bank_dev_read) {
    Bank_Dev *dev = (Bank_Dev *)d;
    Mem_Dev *mem = dev->banks[dev->cpu->mb];
    uint8_t result = mem->read(mem, addr);

    return result;
}

DEVICE_READ16(bank_dev_read16) {
    Bank_Dev *dev = (Bank_Dev *)d;
    Mem_Dev *mem = dev->banks[dev->cpu->mb];
    uint16_t result = mem->read16(mem, addr);

    return result;
}

DEVICE_WRITE(bank_dev_write) {
    Bank_Dev *dev = (Bank_Dev *)d;
    Mem_Dev *mem = dev->banks[dev->cpu->mb];
    uint8_t result = mem->write(mem, addr, val);

    return result;
}

DEVICE_WRITE16(bank_dev_write16) {
    Bank_Dev *dev = (Bank_Dev *)d;
    Mem_Dev *mem = dev->banks[dev->cpu->mb];
    uint8_t result = mem->write16(mem, addr, val);

    return result;
}

DEVICE_PRINT(bank_dev_print) {
    Bank_Dev *dev = (Bank_Dev *)d;
    Mem_Dev *mem = dev->banks[dev->cpu->mb];
    mem->print(mem, addr, label, n);
}

Bank_Dev *
bank_dev_create(size_t num_banks, size_t bank_size, Cpu *cpu) {
    Bank_Dev *result = urq_allocs(Bank_Dev);

    result->load      = bank_dev_load;
    result->read      = bank_dev_read;
    result->read16    = bank_dev_read16;
    result->write     = bank_dev_write;
    result->write16   = bank_dev_write16;
    result->print     = bank_dev_print;

    result->num_banks = num_banks;
    result->bank_size = bank_size;
    result->cpu       = cpu;

    result->banks = (Mem_Dev **)urq_alloc(sizeof(Mem_Dev*) * num_banks);

    for ( int i = 0; i < num_banks; ++i ) {
        uint8_t *mem = (uint8_t *)urq_alloc(bank_size);

        for ( int j = 0; j < bank_size; ++j ) {
            mem[j] = 0;
        }

        result->banks[i] = mem_dev_create(mem, bank_size);
    }

    return result;
}
/* }}} */

