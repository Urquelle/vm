#ifndef __URQ_CPU__
#define __URQ_CPU__

enum Reg {
    REG_ACC,

    REG_R1,
    REG_R2,
    REG_R3,
    REG_R4,
    REG_R5,
    REG_R6,
    REG_R7,
    REG_R8,

    REG_COUNT,
};

struct Cpu {
    Dev      * mem;

    uint16_t   pc; // program counter
    size_t     sp; // stack pointer
    size_t     fp; // frame pointer
    size_t     mb; // memory bank
    size_t     im; // interrupt mask

    uint16_t   stack_frame_size;
    size_t     interrupt_vector_address;
    uint16_t   regs[REG_COUNT];

    bool       in_int;
};

Cpu *
cpu_create(Dev *mem, size_t interrupt_vector_address = 0x1000) {
    Cpu *result = urq_allocs(Cpu);

    *result = {};

    for ( int i = 0; i < REG_COUNT; ++i ) {
        result->regs[i] = 0;
    }

    result->mem = mem;
    result->sp = 0xffff - 2;
    result->fp = result->sp;
    result->mb = 0;
    result->im = 0xffff;
    result->in_int = false;
    result->interrupt_vector_address = interrupt_vector_address;

    return result;
}

#endif

