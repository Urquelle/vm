enum Expr_Kind {
    EXPR_NONE,
    EXPR_IDENT,
    EXPR_IMM,
    EXPR_HEX,
    EXPR_ADDR,
    EXPR_VAR,
    EXPR_REG,
    EXPR_BRACKET,
    EXPR_PAREN,
    EXPR_BIN,
    EXPR_AS,
};

/* @AUFGABE: auf einzelne structs umbauen (siehe sss/parser) */
struct Expr {
    Expr_Kind kind;

    char * file;
    size_t line;
    size_t col;

    union {
        struct {
            char *val;
        } expr_ident;

        struct {
            uint16_t val;
        } expr_imm;

        struct {
            uint16_t val;
        } expr_hex;

        struct {
            Expr *expr;
        } expr_addr;

        struct {
            char *val;
        } expr_var;

        struct {
            char *val;
        } expr_reg;

        struct {
            Expr *expr;
        } expr_bracket;

        struct {
            Expr *expr;
        } expr_paren;

        struct {
            char  op;
            Expr *left;
            Expr *right;
        } expr_bin;

        struct {
            char *structure;
            char *base;
            char *field;
        } expr_as;
    };
};

struct Expr_List {
    std::vector<Expr *> list;
    size_t curr;
};

struct Struct_Field {
    char   * name;
    size_t   size;
};

Struct_Field *
struct_field(char *name, size_t size) {
    Struct_Field *result = urq_allocs(Struct_Field);

    result->name = name;
    result->size = size;

    return result;
}

void
skipnewline(Token_List *tokens) {
    for ( ;; ) {
        if ( !token_match(tokens, T_NEWLINE) ) {
            break;
        }
    }
}

void
expr_push(Expr_List *list, Expr *expr) {
    list->list.push_back(expr);
}

Expr *
expr_new(char *file, size_t line, size_t col, Expr_Kind kind) {
    Expr *result = urq_allocs(Expr);

    result->file = file;
    result->line = line;
    result->col  = col;
    result->kind = kind;

    return result;
}

Expr *
expr_bracket(char *file, size_t line, size_t col, Expr *expr) {
    Expr *result = expr_new(file, line, col, EXPR_BRACKET);

    result->expr_bracket.expr = expr;

    return result;
}

Expr *
expr_paren(char *file, size_t line, size_t col, Expr *expr) {
    Expr *result = expr_new(file, line, col, EXPR_PAREN);

    result->expr_paren.expr = expr;

    return result;
}

Expr *
expr_ident(char *file, size_t line, size_t col, char *val) {
    Expr *result = expr_new(file, line, col, EXPR_IDENT);

    result->expr_ident.val = val;

    return result;
}

Expr *
expr_imm(char *file, size_t line, size_t col, uint16_t val) {
    Expr *result = expr_new(file, line, col, EXPR_IMM);

    result->expr_imm.val = val;

    return result;
}

Expr *
expr_hex(char *file, size_t line, size_t col, uint16_t val) {
    Expr *result = expr_new(file, line, col, EXPR_HEX);

    result->expr_hex.val = val;

    return result;
}

Expr *
expr_addr(char *file, size_t line, size_t col, Expr *expr) {
    Expr *result = expr_new(file, line, col, EXPR_ADDR);

    result->expr_addr.expr = expr;

    return result;
}

Expr *
expr_var(char *file, size_t line, size_t col, char *val) {
    Expr *result = expr_new(file, line, col, EXPR_VAR);

    result->expr_var.val = val;

    return result;
}

Expr *
expr_reg(char *file, size_t line, size_t col, char *val) {
    Expr *result = expr_new(file, line, col, EXPR_REG);

    result->expr_reg.val = val;

    return result;
}

Expr *
expr_bin(char *file, size_t line, size_t col, char op, Expr *left, Expr *right) {
    Expr *result = expr_new(file, line, col, EXPR_BIN);

    result->expr_bin.op    = op;
    result->expr_bin.left  = left;
    result->expr_bin.right = right;

    return result;
}

Expr *
expr_as(char *file, size_t line, size_t col, char *structure, char *base, char *field) {
    Expr *result = expr_new(file, line, col, EXPR_AS);

    result->expr_as.structure = structure;
    result->expr_as.base      = base;
    result->expr_as.field     = field;

    return result;
}

Expr * parse_expr(Token_List *tokens);

Expr *
parse_expr_base(Token_List *tokens) {
    Token *curr_token = token_current(tokens);

    if ( token_match(tokens, T_LBRACKET) ) {
        Expr *expr = parse_expr(tokens);
        token_expect(tokens, T_RBRACKET);

        auto result = expr_bracket(curr_token->file, curr_token->line, curr_token->col, expr);

        return result;
    } else if ( token_match(tokens, T_LT) ) {
        Expr *structure_expr = parse_expr(tokens);
        assert(structure_expr->kind == EXPR_IDENT);
        char *structure = structure_expr->expr_ident.val;

        token_expect(tokens, T_GT);

        Expr *base_expr = parse_expr(tokens);
        assert(base_expr->kind == EXPR_IDENT);
        char *base = base_expr->expr_ident.val;

        token_expect(tokens, T_DOT);

        Expr *field_expr = parse_expr(tokens);
        assert(field_expr->kind == EXPR_IDENT);
        char *field = field_expr->expr_ident.val;

        auto result = expr_as(curr_token->file, curr_token->line, curr_token->col, structure, base, field);

        return result;
    } else if ( token_match(tokens, T_LPAREN) ) {
        Expr *expr = parse_expr(tokens);
        token_expect(tokens, T_RPAREN);

        auto result = expr_paren(curr_token->file, curr_token->line, curr_token->col, expr);

        return result;
    } else if ( token_is(tokens, T_IDENT) ) {
        Token *ident = token_eat(tokens);

        auto result = expr_ident(curr_token->file, curr_token->line, curr_token->col, ident->val_str);

        return result;
    } else if ( token_is(tokens, T_IMM) ) {
        Token *value = token_eat(tokens);

        auto result = expr_imm(curr_token->file, curr_token->line, curr_token->col, value->val_u16);

        return result;
    } else if ( token_is(tokens, T_AMP) ) {
        token_eat(tokens);

        auto result = expr_addr(curr_token->file, curr_token->line, curr_token->col, parse_expr(tokens));

        return result;
    } else if ( token_is(tokens, T_VAR) ) {
        Token *ident = token_eat(tokens);

        auto result = expr_var(curr_token->file, curr_token->line, curr_token->col, ident->val_str);

        return result;
    } else if ( token_is(tokens, T_HEX) ) {
        Token *ident = token_eat(tokens);

        auto result = expr_hex(curr_token->file, curr_token->line, curr_token->col, ident->val_u16);

        return result;
    } else if ( token_is(tokens, T_REG) ) {
        Token *ident = token_eat(tokens);

        auto result = expr_reg(curr_token->file, curr_token->line, curr_token->col, ident->val_str);

        return result;
    }

    return NULL;
}

Expr *
parse_expr_mul_div(Token_List *tokens) {
    Expr *left = parse_expr_base(tokens);

    while ( token_is(tokens, T_MUL) || token_is(tokens, T_DIV) ) {
        if ( token_match(tokens, T_MUL) ) {
            left = expr_bin(left->file, left->line, left->col, '*', left, parse_expr(tokens));
        } else if ( token_match(tokens, T_DIV) ) {
            left = expr_bin(left->file, left->line, left->col, '/', left, parse_expr(tokens));
        }
    }

    return left;
}

Expr *
parse_expr_plus_minus(Token_List *tokens) {
    Expr *left = parse_expr_mul_div(tokens);

    while ( token_is(tokens, T_PLUS) || token_is(tokens, T_MINUS) ) {
        if ( token_match(tokens, T_PLUS) ) {
            left = expr_bin(left->file, left->line, left->col, '+', left, parse_expr(tokens));
        } else if ( token_match(tokens, T_MINUS) ) {
            left = expr_bin(left->file, left->line, left->col, '-', left, parse_expr(tokens));
        }
    }

    return left;
}

Expr *
parse_expr(Token_List *tokens) {
    Expr *expr = parse_expr_plus_minus(tokens);

    return expr;
}

enum Stmt_Kind {
    STMT_NONE,
    STMT_INSTR,
    STMT_DATA8,
    STMT_DATA16,
    STMT_CONST,
    STMT_LABEL,
    STMT_STRUCT,
};

struct Stmt {
    Stmt_Kind kind;

    char * file;
    size_t line;
    size_t col;
    bool   export_stmt;

    union {
        struct {
            uint8_t opcode;
            Format  fmt;

            Expr *op1;
            Expr *op2;
            Expr *op3;
        } stmt_instr;

        struct {
            char * name;
            Queue  elems;
        } stmt_data8;

        struct {
            char * name;
            Queue  elems;
        } stmt_data16;

        struct {
            char * name;
            Expr * val; // @AUFGABE: vielleicht reicht hier auch nur der hex wert
        } stmt_const;

        struct {
            char   * val;
        } stmt_label;

        struct {
            char *name;
            Queue elems;
        } stmt_struct;
    };
};

struct Stmt_List {
    std::vector<Stmt *> list;
    size_t curr;
};

void
stmt_push(Stmt_List *list, Stmt *stmt) {
    list->list.push_back(stmt);
}

Stmt *
stmt_new(char *file, size_t line, size_t col, Stmt_Kind kind) {
    Stmt *result = urq_allocs(Stmt);

    result->file = file;
    result->line = line;
    result->col  = col;
    result->kind = kind;

    return result;
}

Stmt *
stmt_instr(char *file, size_t line, size_t col, uint8_t opcode, Format fmt,
        Expr *op1, Expr *op2, Expr *op3 = NULL)
{
    Stmt *result = stmt_new(file, line, col, STMT_INSTR);

    result->stmt_instr.opcode = opcode;
    result->stmt_instr.fmt    = fmt;
    result->stmt_instr.op1    = op1;
    result->stmt_instr.op2    = op2;
    result->stmt_instr.op3    = op3;

    return result;
}

Stmt *
stmt_const(char *file, size_t line, size_t col, char *name, Expr *val) {
    Stmt *result = stmt_new(file, line, col, STMT_CONST);

    result->stmt_const.name = name;
    result->stmt_const.val  = val;

    return result;
}

Stmt *
stmt_data8(char *file, size_t line, size_t col, char *name, Queue elems) {
    Stmt *result = stmt_new(file, line, col, STMT_DATA8);

    result->stmt_data8.name  = name;
    result->stmt_data8.elems = elems;

    return result;
}

Stmt *
stmt_data16(char *file, size_t line, size_t col, char *name, Queue elems) {
    Stmt *result = stmt_new(file, line, col, STMT_DATA16);

    result->stmt_data16.name  = name;
    result->stmt_data16.elems = elems;

    return result;
}

Stmt *
stmt_label(char *file, size_t line, size_t col, char *val) {
    Stmt *result = stmt_new(file, line, col, STMT_LABEL);

    result->stmt_label.val = val;

    return result;
}

Stmt *
stmt_struct(char *file, size_t line, size_t col, char *name, Queue elems) {
    Stmt *result = stmt_new(file, line, col, STMT_STRUCT);

    result->stmt_struct.name  = name;
    result->stmt_struct.elems = elems;

    return result;
}

Stmt *
parse_stmt_hlt(Token_List *tokens) {
    Token *mov = token_expect(tokens, T_IDENT);

    return stmt_instr(mov->file, mov->line, mov->col, OP_HLT, FMT_NONE, NULL, NULL);
}

Stmt *
parse_stmt_ret(Token_List *tokens) {
    Token *mov = token_expect(tokens, T_IDENT);

    return stmt_instr(mov->file, mov->line, mov->col, OP_RET, FMT_NONE, NULL, NULL);
}

Stmt *
parse_stmt_cal(Token_List *tokens) {
    Token *mov = token_expect(tokens, T_IDENT);

    Expr *op1 = parse_expr(tokens);

    uint8_t opcode = 0;
    Format fmt = FMT_NONE;

    if ( op1->kind == EXPR_IMM || op1->kind == EXPR_BRACKET ) {
        opcode = OP_CAL_IMM;
        fmt = FMT_IMM;
    } else if ( op1->kind == EXPR_REG ) {
        opcode = OP_CAL_REG;
        fmt = FMT_REG;
    } else {
        assert(!"unbekannte anweisung");
    }

    return stmt_instr(mov->file, mov->line, mov->col, opcode, fmt, op1, NULL);
}

Stmt *
parse_stmt_pop(Token_List *tokens) {
    Token *pop = token_expect(tokens, T_IDENT);

    return stmt_instr(pop->file, pop->line, pop->col, OP_POP, FMT_NONE, NULL, NULL);
}

Stmt *
parse_stmt_rti(Token_List *tokens) {
    Token *ident = token_eat(tokens);

    return stmt_instr(ident->file, ident->line, ident->col, OP_RTI, FMT_NONE, NULL, NULL);
}

Stmt *
parse_stmt_psh(Token_List *tokens) {
    Token *mov = token_expect(tokens, T_IDENT);

    Expr *op1 = parse_expr(tokens);
    token_expect(tokens, T_COMMA);
    Expr *op2 = parse_expr(tokens);
    assert(op2->kind == EXPR_ADDR);

    uint8_t opcode = 0;
    Format fmt = FMT_NONE;

    if ( op1->kind == EXPR_IMM || op1->kind == EXPR_BRACKET ) {
        opcode = OP_PSH_IMM;
        fmt = FMT_IMM_MEM;
    } else if ( op1->kind == EXPR_REG ) {
        opcode = OP_PSH_REG;
        fmt = FMT_REG_MEM;
    } else {
        assert(!"unbekannte anweisung");
    }

    return stmt_instr(mov->file, mov->line, mov->col, opcode, fmt, op1, NULL);
}

Stmt *
parse_stmt_jge(Token_List *tokens) {
    Token *mov = token_expect(tokens, T_IDENT);

    Expr *op1 = parse_expr(tokens);
    token_expect(tokens, T_COMMA);
    Expr *op2 = parse_expr(tokens);
    assert(op2->kind == EXPR_ADDR);

    uint8_t opcode = 0;
    Format fmt = FMT_NONE;

    if ( op1->kind == EXPR_IMM || op1->kind == EXPR_BRACKET ) {
        opcode = OP_JGE_IMM;
        fmt = FMT_IMM_MEM;
    } else if ( op1->kind == EXPR_REG ) {
        opcode = OP_JGE_REG;
        fmt = FMT_REG_MEM;
    } else {
        assert(!"unbekannte anweisung");
    }

    return stmt_instr(mov->file, mov->line, mov->col, opcode, fmt, op1, op2);
}

Stmt *
parse_stmt_jle(Token_List *tokens) {
    Token *mov = token_expect(tokens, T_IDENT);

    Expr *op1 = parse_expr(tokens);
    token_expect(tokens, T_COMMA);
    Expr *op2 = parse_expr(tokens);
    assert(op2->kind == EXPR_ADDR);

    uint8_t opcode = 0;
    Format fmt = FMT_NONE;

    if ( op1->kind == EXPR_IMM || op1->kind == EXPR_BRACKET ) {
        opcode = OP_JLE_IMM;
        fmt = FMT_IMM_MEM;
    } else if ( op1->kind == EXPR_REG ) {
        opcode = OP_JLE_REG;
        fmt = FMT_REG_MEM;
    } else {
        assert(!"unbekannte anweisung");
    }

    return stmt_instr(mov->file, mov->line, mov->col, opcode, fmt, op1, op2);
}

Stmt *
parse_stmt_jgt(Token_List *tokens) {
    Token *mov = token_expect(tokens, T_IDENT);

    Expr *op1 = parse_expr(tokens);
    token_expect(tokens, T_COMMA);
    Expr *op2 = parse_expr(tokens);
    assert(op2->kind == EXPR_ADDR);

    uint8_t opcode = 0;
    Format fmt = FMT_NONE;

    if ( op1->kind == EXPR_IMM || op1->kind == EXPR_BRACKET ) {
        opcode = OP_JGT_IMM;
        fmt = FMT_IMM_MEM;
    } else if ( op1->kind == EXPR_REG ) {
        opcode = OP_JGT_REG;
        fmt = FMT_REG_MEM;
    } else {
        assert(!"unbekannte anweisung");
    }

    return stmt_instr(mov->file, mov->line, mov->col, opcode, fmt, op1, op2);
}

Stmt *
parse_stmt_jlt(Token_List *tokens) {
    Token *mov = token_expect(tokens, T_IDENT);

    Expr *op1 = parse_expr(tokens);
    token_expect(tokens, T_COMMA);
    Expr *op2 = parse_expr(tokens);
    assert(op2->kind == EXPR_ADDR);

    uint8_t opcode = 0;
    Format fmt = FMT_NONE;

    if ( op1->kind == EXPR_IMM || op1->kind == EXPR_BRACKET ) {
        opcode = OP_JLT_IMM;
        fmt = FMT_IMM_MEM;
    } else if ( op1->kind == EXPR_REG ) {
        opcode = OP_JLT_REG;
        fmt = FMT_REG_MEM;
    } else {
        assert(!"unbekannte anweisung");
    }

    return stmt_instr(mov->file, mov->line, mov->col, opcode, fmt, op1, op2);
}

Stmt *
parse_stmt_jeq(Token_List *tokens) {
    Token *mov = token_expect(tokens, T_IDENT);

    Expr *op1 = parse_expr(tokens);
    token_expect(tokens, T_COMMA);
    Expr *op2 = parse_expr(tokens);
    assert(op2->kind == EXPR_ADDR);

    uint8_t opcode = 0;
    Format fmt = FMT_NONE;

    if ( op1->kind == EXPR_IMM || op1->kind == EXPR_BRACKET ) {
        opcode = OP_JEQ_IMM;
        fmt = FMT_IMM_MEM;
    } else if ( op1->kind == EXPR_REG ) {
        opcode = OP_JEQ_REG;
        fmt = FMT_REG_MEM;
    } else {
        assert(!"unbekannte anweisung");
    }

    return stmt_instr(mov->file, mov->line, mov->col, opcode, fmt, op1, op2);
}

Stmt *
parse_stmt_jmp(Token_List *tokens) {
    Token *mov = token_expect(tokens, T_IDENT);

    Expr *op1 = parse_expr(tokens);
    assert(op1->kind == EXPR_ADDR);

    uint8_t opcode = OP_JMP;
    Format fmt = FMT_MEM;

    return stmt_instr(mov->file, mov->line, mov->col, opcode, fmt, op1, NULL);
}

Stmt *
parse_stmt_jne(Token_List *tokens) {
    Token *mov = token_expect(tokens, T_IDENT);

    Expr *op1 = parse_expr(tokens);
    token_expect(tokens, T_COMMA);
    Expr *op2 = parse_expr(tokens);
    assert(op2->kind == EXPR_ADDR);

    uint8_t opcode = 0;
    Format fmt = FMT_NONE;

    if ( op1->kind == EXPR_IMM || op1->kind == EXPR_BRACKET ) {
        opcode = OP_JNE_IMM;
        fmt = FMT_IMM_MEM;
    } else if ( op1->kind == EXPR_REG ) {
        opcode = OP_JNE_REG;
        fmt = FMT_REG_MEM;
    } else {
        assert(!"unbekannte anweisung");
    }

    return stmt_instr(mov->file, mov->line, mov->col, opcode, fmt, op1, op2);
}

Stmt *
parse_stmt_xor(Token_List *tokens) {
    Token *mov = token_expect(tokens, T_IDENT);

    Expr *op1 = parse_expr(tokens);
    assert(op1->kind == EXPR_REG);

    token_expect(tokens, T_COMMA);
    Expr *op2 = parse_expr(tokens);

    uint8_t opcode = 0;
    Format fmt = FMT_NONE;

    if ( op2->kind == EXPR_IMM || op2->kind == EXPR_BRACKET ) {
        opcode = OP_XOR_REG_IMM;
        fmt = FMT_REG_IMM;
    } else if ( op1->kind == EXPR_REG ) {
        opcode = OP_XOR_REG_REG;
        fmt = FMT_REG_REG;
    } else {
        assert(!"unbekannte anweisung");
    }

    return stmt_instr(mov->file, mov->line, mov->col, opcode, fmt, op1, op2);
}

Stmt *
parse_stmt_or(Token_List *tokens) {
    Token *mov = token_expect(tokens, T_IDENT);

    Expr *op1 = parse_expr(tokens);
    assert(op1->kind == EXPR_REG);

    token_expect(tokens, T_COMMA);
    Expr *op2 = parse_expr(tokens);

    uint8_t opcode = 0;
    Format fmt = FMT_NONE;

    if ( op2->kind == EXPR_IMM || op2->kind == EXPR_BRACKET ) {
        opcode = OP_OR_REG_IMM;
        fmt = FMT_REG_IMM;
    } else if ( op1->kind == EXPR_REG ) {
        opcode = OP_OR_REG_REG;
        fmt = FMT_REG_REG;
    } else {
        assert(!"unbekannte anweisung");
    }

    return stmt_instr(mov->file, mov->line, mov->col, opcode, fmt, op1, op2);
}

Stmt *
parse_stmt_and(Token_List *tokens) {
    Token *mov = token_expect(tokens, T_IDENT);

    Expr *op1 = parse_expr(tokens);
    assert(op1->kind == EXPR_REG);

    token_expect(tokens, T_COMMA);
    Expr *op2 = parse_expr(tokens);

    uint8_t opcode = 0;
    Format fmt = FMT_NONE;

    if ( op2->kind == EXPR_IMM || op2->kind == EXPR_BRACKET ) {
        opcode = OP_AND_REG_IMM;
        fmt = FMT_REG_IMM;
    } else if ( op1->kind == EXPR_REG ) {
        opcode = OP_AND_REG_REG;
        fmt = FMT_REG_REG;
    } else {
        assert(!"unbekannte anweisung");
    }

    return stmt_instr(mov->file, mov->line, mov->col, opcode, fmt, op1, op2);
}

Stmt *
parse_stmt_not(Token_List *tokens) {
    Token *mov = token_expect(tokens, T_IDENT);

    Expr *op1 = parse_expr(tokens);
    assert(op1->kind == EXPR_REG);

    return stmt_instr(mov->file, mov->line, mov->col, OP_NOT, FMT_REG, op1, NULL);
}

Stmt *
parse_stmt_rsf(Token_List *tokens) {
    Token *mov = token_expect(tokens, T_IDENT);

    Expr *op1 = parse_expr(tokens);
    assert(op1->kind == EXPR_REG);

    token_expect(tokens, T_COMMA);
    Expr *op2 = parse_expr(tokens);

    uint8_t opcode = 0;
    Format fmt = FMT_NONE;

    if ( op2->kind == EXPR_IMM || op2->kind == EXPR_BRACKET ) {
        opcode = OP_RSF_REG_IMM;
        fmt = FMT_REG_IMM;
    } else if ( op1->kind == EXPR_REG ) {
        opcode = OP_RSF_REG_REG;
        fmt = FMT_REG_REG;
    } else {
        assert(!"unbekannte anweisung");
    }

    return stmt_instr(mov->file, mov->line, mov->col, opcode, fmt, op1, op2);
}

Stmt *
parse_stmt_lsf(Token_List *tokens) {
    Token *mov = token_expect(tokens, T_IDENT);

    Expr *op1 = parse_expr(tokens);
    assert(op1->kind == EXPR_REG);

    token_expect(tokens, T_COMMA);
    Expr *op2 = parse_expr(tokens);

    uint8_t opcode = 0;
    Format fmt = FMT_NONE;

    if ( op2->kind == EXPR_IMM || op2->kind == EXPR_BRACKET ) {
        opcode = OP_LSF_REG_IMM;
        fmt = FMT_REG_IMM;
    } else if ( op1->kind == EXPR_REG ) {
        opcode = OP_LSF_REG_REG;
        fmt = FMT_REG_REG;
    } else {
        assert(!"unbekannte anweisung");
    }

    return stmt_instr(mov->file, mov->line, mov->col, opcode, fmt, op1, op2);
}

Stmt *
parse_stmt_dec(Token_List *tokens) {
    Token *mov = token_expect(tokens, T_IDENT);

    Expr *op1 = parse_expr(tokens);
    assert(op1->kind == EXPR_REG);

    return stmt_instr(mov->file, mov->line, mov->col, OP_DEC_REG, FMT_REG, op1, NULL);
}

Stmt *
parse_stmt_inc(Token_List *tokens) {
    Token *mov = token_expect(tokens, T_IDENT);

    Expr *op1 = parse_expr(tokens);
    assert(op1->kind == EXPR_REG);

    return stmt_instr(mov->file, mov->line, mov->col, OP_INC_REG, FMT_REG, op1, NULL);
}

Stmt *
parse_stmt_int(Token_List *tokens) {
    Token *mov = token_expect(tokens, T_IDENT);

    Expr *op1 = parse_expr(tokens);
    assert(op1->kind == EXPR_IMM);

    return stmt_instr(mov->file, mov->line, mov->col, OP_INT, FMT_IMM, op1, NULL);
}

Stmt *
parse_stmt_mul(Token_List *tokens) {
    Token *mov = token_expect(tokens, T_IDENT);

    Expr *op1 = parse_expr(tokens);
    token_expect(tokens, T_COMMA);
    Expr *op2 = parse_expr(tokens);

    uint8_t opcode = 0;
    Format fmt = FMT_NONE;

    if ( op1->kind == EXPR_IMM || op1->kind == EXPR_BRACKET ) {
        if ( op2->kind == EXPR_REG ) {
            opcode = OP_MUL_IMM_REG;
            fmt = FMT_IMM_REG;
        } else {
            assert(!"register erwartet");
        }
    } else if ( op1->kind == EXPR_REG ) {
        if ( op2->kind == EXPR_REG ) {
            opcode = OP_MUL_REG_REG;
            fmt = FMT_REG_REG;
        } else {
            assert(!"register erwartet");
        }
    } else {
        assert(!"unbekannte anweisung");
    }

    return stmt_instr(mov->file, mov->line, mov->col, opcode, fmt, op1, op2);
}

Stmt *
parse_stmt_sub(Token_List *tokens) {
    Token *mov = token_expect(tokens, T_IDENT);

    Expr *op1 = parse_expr(tokens);
    token_expect(tokens, T_COMMA);
    Expr *op2 = parse_expr(tokens);

    uint8_t opcode = 0;
    Format fmt = FMT_NONE;

    if ( op1->kind == EXPR_IMM || op1->kind == EXPR_BRACKET ) {
        if ( op2->kind == EXPR_REG ) {
            opcode = OP_SUB_IMM_REG;
            fmt = FMT_IMM_REG;
        } else {
            assert(!"register erwartet");
        }
    } else if ( op1->kind == EXPR_REG ) {
        if ( op2->kind == EXPR_REG ) {
            opcode = OP_SUB_REG_REG;
            fmt = FMT_REG_REG;
        } else if ( op2->kind == EXPR_IMM || op2->kind == EXPR_BRACKET ) {
            opcode = OP_SUB_REG_IMM;
            fmt = FMT_REG_IMM;
        }
    } else {
        assert(!"unbekannte anweisung");
    }

    return stmt_instr(mov->file, mov->line, mov->col, opcode, fmt, op1, op2);
}

Stmt *
parse_stmt_add(Token_List *tokens) {
    Token *mov = token_expect(tokens, T_IDENT);

    Expr *op1 = parse_expr(tokens);
    token_expect(tokens, T_COMMA);
    Expr *op2 = parse_expr(tokens);

    uint8_t opcode = 0;
    Format fmt = FMT_NONE;

    if ( op1->kind == EXPR_IMM || op1->kind == EXPR_BRACKET ) {
        if ( op2->kind == EXPR_REG ) {
            opcode = OP_ADD_IMM_REG;
            fmt = FMT_IMM_REG;
        } else {
            assert(!"register erwartet");
        }
    } else if ( op1->kind == EXPR_REG ) {
        if ( op2->kind == EXPR_REG ) {
            opcode = OP_ADD_REG_REG;
            fmt = FMT_REG_REG;
        } else {
            assert(!"register erwartet");
        }
    } else {
        assert(!"unbekannte anweisung");
    }

    return stmt_instr(mov->file, mov->line, mov->col, opcode, fmt, op1, op2);
}

Stmt *
parse_stmt_mov(Token_List *tokens) {
    Token *mov = token_expect(tokens, T_IDENT);

    Expr *op1 = parse_expr(tokens);
    token_expect(tokens, T_COMMA);
    Expr *op2 = parse_expr(tokens);

    Expr *op3 = NULL;
    if ( token_match(tokens, T_COMMA) ) {
        op3 = parse_expr(tokens);
    }

    uint8_t opcode = 0;
    Format fmt = FMT_NONE;

    if ( op1->kind == EXPR_IMM || op1->kind == EXPR_BRACKET ) {
        if ( op2->kind == EXPR_REG ) {
            opcode = OP_MOV_IMM_REG;
            fmt = FMT_IMM_REG;
        } else if ( op2->kind == EXPR_ADDR && !op3 ) {
            opcode = OP_MOV_IMM_MEM;
            fmt = FMT_IMM_MEM;
        } else {
            assert(op2->kind == EXPR_ADDR && op3 != NULL);

            if ( op2->expr_addr.expr->kind != EXPR_REG ) {
                assert(!"register erwartet");
            }

            op2 = op2->expr_addr.expr;
            opcode = OP_MOV_IMM_OFF_REG;
            fmt = FMT_IMM_OFF_REG;
        }
    } else if ( op1->kind == EXPR_ADDR && op1->expr_addr.expr->kind == EXPR_REG ) {
        op1 = op1->expr_addr.expr;

        if ( op2->kind == EXPR_REG ) {
            opcode = OP_MOV_REG_PTR_REG;
            fmt = FMT_REG_PTR_REG;
        } else {
            assert(!"register erwartet");
        }
    } else if ( op1->kind == EXPR_REG ) {
        if ( op2->kind == EXPR_REG ) {
            opcode = OP_MOV_REG_REG;
            fmt = FMT_REG_REG;
        } else if ( op2->kind == EXPR_ADDR ) {
            opcode = OP_MOV_REG_MEM;
            fmt = FMT_REG_MEM;
        }
    } else if ( op1->kind == EXPR_ADDR ) {
        assert(op2->kind == EXPR_REG);
        opcode = OP_MOV_MEM_REG;
        fmt = FMT_MEM_REG;
    } else {
        assert(!"unbekannte anweisung");
    }

    return stmt_instr(mov->file, mov->line, mov->col, opcode, fmt, op1, op2, op3);
}

Stmt *
parse_stmt_const(Token_List *tokens) {
    Token *ident = token_eat(tokens);

    Expr *name = parse_expr(tokens);
    assert(name->kind == EXPR_IDENT);
    token_expect(tokens, T_ASSIGN);
    Expr *val = parse_expr(tokens);

    return stmt_const(ident->file, ident->line, ident->col, name->expr_ident.val, val);
}

Stmt *
parse_stmt_data8(Token_List *tokens) {
    Token *ident = token_eat(tokens);

    Expr *name = parse_expr(tokens);
    assert(name->kind == EXPR_IDENT);
    token_expect(tokens, T_ASSIGN);
    token_expect(tokens, T_LBRACE);

    Queue elems = {};
    if ( !token_is(tokens, T_RBRACE) ) {
        do {
            Expr *expr = parse_expr(tokens);
            queue_push(&elems, expr);
        } while ( token_match(tokens, T_COMMA) );
    }

    token_expect(tokens, T_RBRACE);

    return stmt_data8(ident->file, ident->line, ident->col, name->expr_ident.val, elems);
}

Stmt *
parse_stmt_data16(Token_List *tokens) {
    Token *ident = token_eat(tokens);

    Expr *name = parse_expr(tokens);
    assert(name->kind == EXPR_IDENT);
    token_expect(tokens, T_ASSIGN);
    token_expect(tokens, T_LBRACE);

    Queue elems = {};
    if ( !token_is(tokens, T_RBRACE) ) {
        do {
            Expr *expr = parse_expr(tokens);
            queue_push(&elems, expr);
        } while ( token_match(tokens, T_COMMA) );
    }

    token_expect(tokens, T_RBRACE);

    return stmt_data16(ident->file, ident->line, ident->col, name->expr_ident.val, elems);
}

Stmt *
parse_stmt_label(Token_List *tokens) {
    Token *ident = token_eat(tokens);

    token_expect(tokens, T_COLON);
    return stmt_label(ident->file, ident->line, ident->col, ident->val_str);
}

Stmt *
parse_stmt_struct(Token_List *tokens) {
    Token *_ = token_eat(tokens);

    Expr *name_expr = parse_expr(tokens);
    assert(name_expr->kind == EXPR_IDENT);
    char *name = name_expr->expr_ident.val;
    token_expect(tokens, T_LBRACE);
    skipnewline(tokens);

    Queue elems = {};
    if ( !token_is(tokens, T_RBRACE) ) {
        do {
            skipnewline(tokens);
            Expr *field_name = parse_expr(tokens);
            assert(field_name->kind == EXPR_IDENT);
            skipnewline(tokens);

            token_expect(tokens, T_COLON);
            skipnewline(tokens);

            Expr *field_size = parse_expr(tokens);
            assert(field_size->kind == EXPR_IMM);
            skipnewline(tokens);

            queue_push(&elems, struct_field(field_name->expr_ident.val, field_size->expr_imm.val));

            token_match(tokens, T_COMMA);
            skipnewline(tokens);
        } while ( !token_is(tokens, T_RBRACE) );
    }

    skipnewline(tokens);
    token_expect(tokens, T_RBRACE);

    return stmt_struct(_->file, _->line, _->col, name, elems);
}

Stmt *
parse_stmt(Token_List *tokens) {
    Token *curr_token = token_current(tokens);
    Stmt *result = NULL;

    bool export_stmt = false;
    if ( token_match(tokens, T_PLUS) ) {
        export_stmt = true;
    }

    if ( curr_token->val_str == instr_mov ) {
        result = parse_stmt_mov(tokens);
    } else if ( curr_token->val_str == instr_add ) {
        result = parse_stmt_add(tokens);
    } else if ( curr_token->val_str == instr_sub ) {
        result = parse_stmt_sub(tokens);
    } else if ( curr_token->val_str == instr_mul ) {
        result = parse_stmt_mul(tokens);
    } else if ( curr_token->val_str == instr_inc ) {
        result = parse_stmt_inc(tokens);
    } else if ( curr_token->val_str == instr_dec ) {
        result = parse_stmt_dec(tokens);
    } else if ( curr_token->val_str == instr_lsf ) {
        result = parse_stmt_lsf(tokens);
    } else if ( curr_token->val_str == instr_rsf ) {
        result = parse_stmt_rsf(tokens);
    } else if ( curr_token->val_str == instr_and ) {
        result = parse_stmt_and(tokens);
    } else if ( curr_token->val_str == instr_or ) {
        result = parse_stmt_or(tokens);
    } else if ( curr_token->val_str == instr_xor ) {
        result = parse_stmt_xor(tokens);
    } else if ( curr_token->val_str == instr_not ) {
        result = parse_stmt_not(tokens);
    } else if ( curr_token->val_str == instr_jmp ) {
        result = parse_stmt_jmp(tokens);
    } else if ( curr_token->val_str == instr_jne ) {
        result = parse_stmt_jne(tokens);
    } else if ( curr_token->val_str == instr_jeq ) {
        result = parse_stmt_jeq(tokens);
    } else if ( curr_token->val_str == instr_jlt ) {
        result = parse_stmt_jlt(tokens);
    } else if ( curr_token->val_str == instr_jgt ) {
        result = parse_stmt_jgt(tokens);
    } else if ( curr_token->val_str == instr_jle ) {
        result = parse_stmt_jle(tokens);
    } else if ( curr_token->val_str == instr_jge ) {
        result = parse_stmt_jge(tokens);
    } else if ( curr_token->val_str == instr_psh ) {
        result = parse_stmt_psh(tokens);
    } else if ( curr_token->val_str == instr_pop ) {
        result = parse_stmt_pop(tokens);
    } else if ( curr_token->val_str == instr_cal ) {
        result = parse_stmt_cal(tokens);
    } else if ( curr_token->val_str == instr_ret ) {
        result = parse_stmt_ret(tokens);
    } else if ( curr_token->val_str == instr_hlt ) {
        result = parse_stmt_hlt(tokens);
    } else if ( curr_token->val_str == instr_int ) {
        result = parse_stmt_int(tokens);
    } else if ( curr_token->val_str == instr_rti ) {
        result = parse_stmt_rti(tokens);
    } else if ( keyword_is(tokens, keyword_data8) ) {
        result = parse_stmt_data8(tokens);
    } else if ( keyword_is(tokens, keyword_data16) ) {
        result = parse_stmt_data16(tokens);
    } else if ( keyword_is(tokens, keyword_const) ) {
        result = parse_stmt_const(tokens);
    } else if ( keyword_is(tokens, keyword_struct) ) {
        result = parse_stmt_struct(tokens);
    } else if ( curr_token->kind == T_IDENT ) {
        result = parse_stmt_label(tokens);
    }

    if ( result ) {
        if ( export_stmt &&
             result->kind != STMT_DATA16 &&
             result->kind != STMT_DATA8  &&
             result->kind != STMT_CONST  &&
             result->kind != STMT_STRUCT)
        {
            assert(!"unerlaubte export anweisung");
        }

        result->export_stmt = export_stmt;
    }

    return result;
}

Stmt_List
parse(Token_List *tokens) {
    Stmt_List result = {};

    skipnewline(tokens);
    Stmt *stmt = parse_stmt(tokens);
    while ( stmt ) {
        stmt_push(&result, stmt);
        skipnewline(tokens);
        stmt = parse_stmt(tokens);
        skipnewline(tokens);
    }

    return result;
}

