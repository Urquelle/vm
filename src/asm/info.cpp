#ifndef __URQ_VM_INFO__
#define __URQ_VM_INFO__

enum Format {
    FMT_NONE,
    FMT_REG_PTR_REG,
    FMT_IMM_OFF_REG,
    FMT_IMM_REG,
    FMT_IMM_MEM,
    FMT_REG_IMM,
    FMT_REG_IMM8, /* wofür? */
    FMT_REG_REG,
    FMT_REG_MEM,
    FMT_MEM_REG,
    FMT_MEM,
    FMT_REG,
    FMT_IMM,
};

#endif

