#ifndef __URQ_ASM__
#define __URQ_ASM__

namespace Urq { namespace Asm {

using namespace Urq::Vm;

#include "info.cpp"
#include "lex.cpp"
#include "parser.cpp"
#include "bytecode.cpp"

namespace api {
    using Urq::Asm::Token;

    using Urq::Asm::bytecode;
    using Urq::Asm::parse;
    using Urq::Asm::tokenize;

    using Urq::Asm::label_addr;
    using Urq::Asm::var_addr;

    using Urq::Asm::reg_r1;
    using Urq::Asm::reg_r2;
    using Urq::Asm::reg_r3;
    using Urq::Asm::reg_r4;
    using Urq::Asm::reg_r5;
    using Urq::Asm::reg_r6;
    using Urq::Asm::reg_r7;
    using Urq::Asm::reg_r8;
    using Urq::Asm::reg_acc;

    using Urq::Asm::keyword_data8;
    using Urq::Asm::keyword_data16;
    using Urq::Asm::keyword_const;
    using Urq::Asm::keyword_struct;

    using Urq::Asm::instr_mov;
    using Urq::Asm::instr_add;
    using Urq::Asm::instr_sub;
    using Urq::Asm::instr_mul;
    using Urq::Asm::instr_inc;
    using Urq::Asm::instr_dec;
    using Urq::Asm::instr_lsf;
    using Urq::Asm::instr_rsf;
    using Urq::Asm::instr_and;
    using Urq::Asm::instr_or;
    using Urq::Asm::instr_xor;
    using Urq::Asm::instr_not;
    using Urq::Asm::instr_jmp;
    using Urq::Asm::instr_jne;
    using Urq::Asm::instr_jeq;
    using Urq::Asm::instr_jlt;
    using Urq::Asm::instr_jgt;
    using Urq::Asm::instr_jle;
    using Urq::Asm::instr_jge;
    using Urq::Asm::instr_psh;
    using Urq::Asm::instr_pop;
    using Urq::Asm::instr_cal;
    using Urq::Asm::instr_ret;
    using Urq::Asm::instr_hlt;
    using Urq::Asm::instr_int;
    using Urq::Asm::instr_rti;

    using Urq::Asm::regs;
    using Urq::Asm::tokenize;
    using Urq::Asm::token_match;
}

}}

#endif

