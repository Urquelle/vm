Map regs;
Map keywords;
Map instructions;
Map interns;

char *reg_r1;
char *reg_r2;
char *reg_r3;
char *reg_r4;
char *reg_r5;
char *reg_r6;
char *reg_r7;
char *reg_r8;
char *reg_acc;

char *keyword_data8;
char *keyword_data16;
char *keyword_const;
char *keyword_struct;

char *instr_mov;
char *instr_add;
char *instr_sub;
char *instr_mul;
char *instr_inc;
char *instr_dec;
char *instr_lsf;
char *instr_rsf;
char *instr_and;
char *instr_or;
char *instr_xor;
char *instr_not;
char *instr_jmp;
char *instr_jne;
char *instr_jeq;
char *instr_jlt;
char *instr_jgt;
char *instr_jle;
char *instr_jge;
char *instr_psh;
char *instr_pop;
char *instr_cal;
char *instr_ret;
char *instr_hlt;
char *instr_int;
char *instr_rti;

enum Token_Kind {
    T_NONE,
    T_NEWLINE,
    T_DOLLAR,
    T_HASH,
    T_COMMA,
    T_NUM,
    T_AMP,
    T_COLON,
    T_LT,
    T_GT,
    T_DOT,

    T_HEX,
    T_REG,
    T_IDENT,
    T_VAR,
    T_IMM,

    T_LBRACKET,
    T_RBRACKET,
    T_LPAREN,
    T_RPAREN,
    T_LBRACE,
    T_RBRACE,

    T_PLUS,
    T_MINUS,
    T_MUL,
    T_DIV,

    T_ASSIGN,
};

struct Token {
    Token_Kind kind;

    char *   val_str;
    uint16_t val_u16;

    char * file;
    size_t line;
    size_t col;
};

struct Token_List {
    std::vector<Token *> list;
    size_t curr;
};

char *
intern_str(char *value) {
    return intern_str(&interns, value);
}

bool
isnum(char c) {
    bool result = c >= '0' && c <= '9';

    return result;
}

bool
isalpha(char c) {
    bool result = c >= 'a' && c <= 'z' || c >= 'A' && c <= 'Z';

    return result;
}

char *
isreg(char *str) {
    size_t len = strlen(str);
    uint64_t hash = bytes_hash(str, len);
    void *key = (void *)(uintptr_t)(hash ? hash : 1);

    Intern *intern = (Intern *)map_get(&regs, key);
    for (Intern *it = intern; it; it = it->next) {
        if (it->len == len && strncmp(it->str, str, len) == 0) {
            return it->str;
        }
    }

    return NULL;
}

bool
ishex(char c) {
    bool result = c >= '0' && c <= '9' || c >= 'a' && c <= 'f' || c >= 'A' && c <= 'F';

    return result;
}

char *
iskeyword(char *str) {
    size_t len = strlen(str);
    uint64_t hash = bytes_hash(str, len);
    void *key = (void *)(uintptr_t)(hash ? hash : 1);

    Intern *intern = (Intern *)map_get(&keywords, key);
    for (Intern *it = intern; it; it = it->next) {
        if (it->len == len && strncmp(it->str, str, len) == 0) {
            return it->str;
        }
    }

    return NULL;
}

Token *
token_create(Token_Kind kind, char *val, char *file, size_t line, size_t col) {
    Token *result = urq_allocs(Token);

    result->kind = kind;

    result->val_str = val;

    result->file = file;
    result->line = line;
    result->col  = col;

    return result;
}

Token *
token_create(Token_Kind kind, uint16_t val, char *file, size_t line, size_t col) {
    Token *result = urq_allocs(Token);

    result->kind = kind;

    result->val_u16 = val;

    result->file = file;
    result->line = line;
    result->col  = col;

    return result;
}

void
token_push(Token_List *list, Token *token) {
    list->list.push_back(token);
}

char *
token_val(char *val, size_t len, bool lower = true) {
    char *result = (char *)urq_alloc(len+1);
    memcpy(result, val, len);
    result[len] = 0;

    if ( lower ) {
        for ( int i = 0; i < len; ++i ) {
            result[i] = (char)std::tolower(result[i]);
        }
    }

    result = intern_str(&interns, result);

    char *reg = isreg(result);
    if ( reg ) {
        return reg;
    }

    char *keyword = iskeyword(result);
    if ( keyword ) {
        return keyword;
    }

    return result;
}

uint16_t
lex_hexdec(char *num) {
    int base = 1;
    int len = (int)strlen(num);
    uint16_t result = 0;

    for ( int i = len - 1; i >= 0; --i ) {
        if ( num[i] >= '0' && num[i] <= '9') {
            result += (uint16_t)((num[i] - 48)*base);
            base = base * 16;
        } else if ( num[i] >= 'a' && num[i] <= 'f') {
            result += (uint16_t)((num[i] - 87)*base);
            base = base*16;
        }
    }

    return result;
}

Token_List
tokenize(char *file, char *input) {
#define NEXT() do { c += utf8_char_size(c); } while(false)
#define AT(N) (*(c+N) ? *(c+N) : 0)

    reg_r1  = intern_str(&regs, "r1");
    reg_r2  = intern_str(&regs, "r2");
    reg_r3  = intern_str(&regs, "r3");
    reg_r4  = intern_str(&regs, "r4");
    reg_r5  = intern_str(&regs, "r5");
    reg_r6  = intern_str(&regs, "r6");
    reg_r7  = intern_str(&regs, "r7");
    reg_r8  = intern_str(&regs, "r8");
    reg_acc = intern_str(&regs, "acc");

    keyword_data8  = intern_str(&keywords, "data8");
    keyword_data16 = intern_str(&keywords, "data16");
    keyword_const  = intern_str(&keywords, "const");
    keyword_struct = intern_str(&keywords, "struct");

    instr_mov = intern_str("mov");
    instr_add = intern_str("add");
    instr_sub = intern_str("sub");
    instr_mul = intern_str("mul");
    instr_inc = intern_str("inc");
    instr_dec = intern_str("dec");
    instr_lsf = intern_str("lsf");
    instr_rsf = intern_str("rsf");
    instr_and = intern_str("and");
    instr_or  = intern_str("or");
    instr_xor = intern_str("xor");
    instr_not = intern_str("not");
    instr_jmp = intern_str("jmp");
    instr_jne = intern_str("jne");
    instr_jeq = intern_str("jeq");
    instr_jlt = intern_str("jlt");
    instr_jgt = intern_str("jgt");
    instr_jle = intern_str("jle");
    instr_jge = intern_str("jge");
    instr_psh = intern_str("psh");
    instr_pop = intern_str("pop");
    instr_cal = intern_str("cal");
    instr_ret = intern_str("ret");
    instr_hlt = intern_str("hlt");
    instr_int = intern_str("int");
    instr_rti = intern_str("rti");

    Token_List result = {};

    char *c = input;
    size_t line = 1;
    size_t col = 1;

    for (;;) {
retry:
        while ( AT(0) == ' ' || AT(0) == '\t' ) {
            col++;
            NEXT();
        }

        /* @INFO: kommentar */
        if ( AT(0) == '\'' ) {
            while ( AT(0) != '\n' ) {
                NEXT();
            }

            goto retry;
        }

        if ( AT(0) == 0 ) {
            break;
        } else if ( AT(0) == '\n') {
            NEXT();
            token_push(&result, token_create(T_NEWLINE, "\n", file, line, col));
            col++;
        } else if ( AT(0) == '=' ) {
            NEXT();
            token_push(&result, token_create(T_ASSIGN, "=", file, line, col));
            col++;
        } else if ( AT(0) == ':' ) {
            NEXT();
            token_push(&result, token_create(T_COLON, ":", file, line, col));
            col++;
        } else if ( AT(0) == '<' ) {
            NEXT();
            token_push(&result, token_create(T_LT, "<", file, line, col));
            col++;
        } else if ( AT(0) == '>' ) {
            NEXT();
            token_push(&result, token_create(T_GT, ">", file, line, col));
            col++;
        } else if ( AT(0) == '.' ) {
            NEXT();
            token_push(&result, token_create(T_DOT, ".", file, line, col));
            col++;
        } else if ( AT(0) == ',' ) {
            NEXT();
            token_push(&result, token_create(T_COMMA, ",", file, line, col));
            col++;
        } else if ( AT(0) == '[' ) {
            NEXT();
            token_push(&result, token_create(T_LBRACKET, "[", file, line, col));
            col++;
        } else if ( AT(0) == ']' ) {
            NEXT();
            token_push(&result, token_create(T_RBRACKET, "]", file, line, col));
            col++;
        } else if ( AT(0) == '(' ) {
            NEXT();
            token_push(&result, token_create(T_LPAREN, "(", file, line, col));
            col++;
        } else if ( AT(0) == ')' ) {
            NEXT();
            token_push(&result, token_create(T_RPAREN, ")", file, line, col));
            col++;
        } else if ( AT(0) == '{' ) {
            NEXT();
            token_push(&result, token_create(T_LBRACE, "{", file, line, col));
            col++;
        } else if ( AT(0) == '}' ) {
            NEXT();
            token_push(&result, token_create(T_RBRACE, "}", file, line, col));
            col++;
        } else if ( AT(0) == '+' ) {
            NEXT();
            token_push(&result, token_create(T_PLUS, "+", file, line, col));
            col++;
        } else if ( AT(0) == '-' ) {
            NEXT();
            token_push(&result, token_create(T_MINUS, "-", file, line, col));
            col++;
        } else if ( AT(0) == '*' ) {
            NEXT();
            token_push(&result, token_create(T_MUL, "*", file, line, col));
            col++;
        } else if ( AT(0) == '/' ) {
            NEXT();
            token_push(&result, token_create(T_DIV, "/", file, line, col));
            col++;
        } else if ( AT(0) == '$' ) {
            NEXT();
            char *start = c;

            while ( ishex(AT(0)) ) {
                NEXT();
            }

            size_t len = c - start;
            char *val = token_val(start, len);
            uint16_t val_u16 = lex_hexdec(val);

            token_push(&result, token_create(T_IMM, val_u16, file, line, col));
            col += len;
        } else if ( AT(0) == '&' ) {
            NEXT();
            token_push(&result, token_create(T_AMP, "&", file, line, col));
            col++;
        } else if ( AT(0) == '!' ) {
            NEXT();
            char *start = c;

            if ( !(AT(0) == '_' || isalpha(AT(0))) ) {
                token_push(&result, token_create(T_NONE, "unbekanntes zeichen entdeckt", file, line, col));
                break;
            }

            NEXT();
            while ( AT(0) == '_' || isalpha(AT(0)) || isnum(AT(0)) ) {
                NEXT();
            }

            size_t len = c - start;
            char *val = token_val(start, len, false);
            token_push(&result, token_create(T_VAR, val, file, line, col));
            col += len;
        } else if ( isalpha(AT(0)) || AT(0) == '_' ) {
            char *start = c;
            NEXT();

            while ( isalpha(AT(0)) || isnum(AT(0)) || AT(0) == '_' ) {
                NEXT();
            }

            size_t len = c - start;
            char *temp_val = token_val(start, len);

            if ( isreg(temp_val) ) {
                token_push(&result, token_create(T_REG, temp_val, file, line, col));
            } else {
                char *val = token_val(start, len, false);
                token_push(&result, token_create(T_IDENT, val, file, line, col));
            }

            col += len;
        } else if ( ishex(AT(0)) ) {
            char *start = c;
            NEXT();

            while ( ishex(AT(0)) ) {
                NEXT();
            }

            size_t len = c - start;
            char *val = token_val(start, len);
            uint16_t val_u16 = lex_hexdec(val);

            token_push(&result, token_create(T_HEX, val_u16, file, line, col));
            col += len;
        } else {
            token_push(&result, token_create(T_NONE, "unbekanntes zeichen entdeckt", file, line, col));
            break;
        }
    }

    return result;

#undef AT
#undef NEXT
}

Token empty_token_str = {};
Token * empty_token = &empty_token_str;

Token *
token_current(Token_List *tokens) {
    if ( tokens->curr >= tokens->list.size() ) {
        return empty_token;
    }

    Token *result = tokens->list[tokens->curr];

    return result;
}

Token *
token_eat(Token_List *tokens) {
    if ( tokens->curr >= tokens->list.size() ) {
        return empty_token;
    }

    return tokens->list[tokens->curr++];
}

Token *
token_expect(Token_List *tokens, Token_Kind expected_kind) {
    Token *token = token_current(tokens);

    if ( token->kind != expected_kind ) {
        assert(!"falsches token");
        return 0;
    }

    return token_eat(tokens);
}

bool
token_is(Token_List *tokens, Token_Kind expected_kind) {
    Token *token = token_current(tokens);

    if ( token->kind == expected_kind ) {
        return true;
    }

    return false;
}

bool
token_match(Token_List *tokens, Token_Kind expected_kind) {
    if ( token_is(tokens, expected_kind) ) {
        token_eat(tokens);

        return true;
    }

    return false;
}

bool
keyword_is(Token_List *tokens, char *keyword) {
    Token *curr_token = token_current(tokens);

    if ( curr_token->kind == T_IDENT && curr_token->val_str == keyword ) {
        return true;
    }

    return false;
}

bool
keyword_match(Token_List *tokens, char *keyword) {
    if ( keyword_is(tokens, keyword) ) {
        token_eat(tokens);

        return true;
    }

    return false;
}

