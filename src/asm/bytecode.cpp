Map syms;
Map structs;
Map deps;

struct Program {
    uint8_t *code;
    size_t size;
};

struct Ref {
    size_t   addr;
    Ref    * next;
};

struct Bytecode_Field {
    char   * name;
    size_t   size;
    size_t   offset;
};

struct Bytecode_Struct {
    char * name;
    Map    fields;
};

void
sym_add(char *name, Ref *ref) {
    if ( map_get(&syms, name) ) {
        assert(!"symbol bereits vorhanden");
    }

    map_put(&syms, name, ref);
}

void
sym_resolve16(uint8_t *buf, char *name, uint16_t val) {
    Ref *ref = (Ref *)map_get(&deps, name);

    while ( ref ) {
        bit_write16(buf, ref->addr, val);
        ref = ref->next;
    }
}

void
sym_resolve8(uint8_t *buf, char *name, uint8_t val) {
    Ref *ref = (Ref *)map_get(&deps, name);
    while ( ref ) {
        bit_write8(buf, ref->addr, val);
        ref = ref->next;
    }
}

Ref *
ref(size_t addr) {
    Ref *result = urq_allocs(Ref);

    result->addr  = addr;
    result->next  = NULL;

    return result;
}

Program *
program(uint8_t *code, size_t size) {
    Program *result = urq_allocs(Program);

    result->code = code;
    result->size = size;

    return result;
}

Bytecode_Struct *
bytecode_struct(char *name) {
    Bytecode_Struct *result = urq_allocs(Bytecode_Struct);

    result->name = name;
    result->fields = {};

    return result;
}

Bytecode_Field *
bytecode_field(char *name, size_t size, size_t offset) {
    Bytecode_Field *result = urq_allocs(Bytecode_Field);

    result->name = name;
    result->size = size;
    result->offset = offset;

    return result;
}

uint16_t
label_addr(char *sym) {
    Ref *s = (Ref *)map_get(&syms, intern_str(sym));

    if ( s ) {
        return (uint16_t)s->addr;
    }

    assert(!"label nicht gefunden");
    return 0;
}

uint16_t
var_addr(char *sym) {
    Ref *s = (Ref *)map_get(&syms, intern_str(sym));

    if ( s ) {
        return (uint16_t)s->addr;
    }

    assert(!"variable nicht gefunden");
    return 0;
}

uint8_t
bytecode_reg(char *val) {
    if ( val == reg_r1 ) {
        return REG_R1;
    } else if ( val == reg_r2 ) {
        return REG_R2;
    } else if ( val == reg_r3 ) {
        return REG_R3;
    } else if ( val == reg_r4 ) {
        return REG_R4;
    } else if ( val == reg_r5 ) {
        return REG_R5;
    } else if ( val == reg_r6 ) {
        return REG_R6;
    } else if ( val == reg_r7 ) {
        return REG_R7;
    } else if ( val == reg_r8 ) {
        return REG_R8;
    } else if ( val == reg_acc ) {
        return REG_ACC;
    } else {
        assert(!"unbekanntes register");
        return 0;
    }
}

size_t
bytecode_emit(uint8_t *buf, size_t ptr, uint8_t val) {
    size_t result = bit_write8(buf, ptr, val);

    return 1;
}

size_t
bytecode_emit16(uint8_t *buf, size_t ptr, uint16_t val) {
    size_t result = bit_write16(buf, ptr, val);

    return result;
}

uint16_t
bytecode_expr16(size_t ptr, Expr *expr) {
    switch ( expr->kind ) {
        case EXPR_IMM: {
            return expr->expr_imm.val;
        } break;

        case EXPR_HEX: {
            return expr->expr_hex.val;
        } break;

        case EXPR_VAR: {
            char *val = expr->expr_var.val;

            Ref *ref_ptr = (Ref *)map_get(&syms, val);

            if ( !ref_ptr ) {
                Ref *root = (Ref *)map_get(&deps, val);

                if ( !root ) {
                    map_put(&deps, val, ref(ptr));
                } else {
                    Ref *r = root;
                    for ( ;; ) {
                        if ( !r->next ) {
                            break;
                        }

                        r = r->next;
                    }

                    r->next = ref(ptr);
                }

                return 0;
            } else {
                return (uint16_t)ref_ptr->addr;
            }
        } break;

        case EXPR_AS: {
            Bytecode_Struct *structure = (Bytecode_Struct *)map_get(&structs, expr->expr_as.structure);
            Bytecode_Field *field = (Bytecode_Field *)map_get(&structure->fields, expr->expr_as.field);
            Ref *base = (Ref *)map_get(&syms, expr->expr_as.base);

            return (uint16_t)(base->addr + field->offset);
        } break;

        case EXPR_REG: {
            return bytecode_reg(expr->expr_reg.val);
        } break;

        case EXPR_ADDR: {
            return bytecode_expr16(ptr, expr->expr_addr.expr);
        } break;

        case EXPR_BRACKET: {
            return bytecode_expr16(ptr, expr->expr_bracket.expr);
        } break;

        case EXPR_BIN: {
            uint16_t left  = bytecode_expr16(ptr, expr->expr_bin.left);
            uint16_t right = bytecode_expr16(ptr, expr->expr_bin.right);

            switch ( expr->expr_bin.op ) {
                case '+': {
                    return left + right;
                } break;

                case '-': {
                    return left - right;
                } break;

                case '*': {
                    return left * right;
                } break;

                default: {
                    assert(!"unbekannte operation");
                } break;
            }
        } break;

        default: {
            assert(!"unbekannter ausdruck");
            return 0;
        } break;
    }

    assert(!"hier dürften wir nicht hinkommen");
    return 0;
}

size_t
bytecode_emit_imm(uint8_t *buf, size_t ptr, Expr *expr) {
    uint16_t imm = bytecode_expr16(ptr, expr);

    return bytecode_emit16(buf, ptr, imm);
}

size_t
bytecode_emit_reg(uint8_t *buf, size_t ptr, Expr *expr) {
    assert(expr->kind == EXPR_REG);

    uint8_t reg = (uint8_t)bytecode_expr16(ptr, expr);
    bytecode_emit(buf, ptr, reg);

    return 1;
}

size_t
bytecode_emit_addr(uint8_t *buf, size_t ptr, Expr *expr) {
    uint16_t addr = bytecode_expr16(ptr, expr);
    bytecode_emit16(buf, ptr, addr);

    return 2;
}

Program *
bytecode(Stmt_List *stmts) {
    /* @AUFGABE: dynamisch anwachsenden speicher implementieren */
    uint8_t *buf = (uint8_t *)urq_alloc(256*256);
    size_t ptr = 0;

    for ( int i = 0; i < stmts->list.size(); ++i ) {
        Stmt *stmt = stmts->list[i];

        if ( stmt->kind == STMT_INSTR ) {
            ptr += bytecode_emit(buf, ptr, stmt->stmt_instr.opcode);

            switch ( stmt->stmt_instr.fmt ) {
                case FMT_NONE: {
                } break;

                case FMT_REG_PTR_REG: {
                    ptr += bytecode_emit_reg(buf, ptr, stmt->stmt_instr.op1);
                    ptr += bytecode_emit_reg(buf, ptr, stmt->stmt_instr.op2);
                } break;

                case FMT_IMM_OFF_REG: {
                    ptr += bytecode_emit_imm(buf, ptr, stmt->stmt_instr.op1);
                    ptr += bytecode_emit_reg(buf, ptr, stmt->stmt_instr.op2);
                    ptr += bytecode_emit_reg(buf, ptr, stmt->stmt_instr.op3);
                } break;

                case FMT_IMM_REG: {
                    ptr += bytecode_emit_imm(buf, ptr, stmt->stmt_instr.op1);
                    ptr += bytecode_emit_reg(buf, ptr, stmt->stmt_instr.op2);
                } break;

                case FMT_IMM_MEM: {
                    ptr += bytecode_emit_imm(buf, ptr, stmt->stmt_instr.op1);
                    ptr += bytecode_emit_addr(buf, ptr, stmt->stmt_instr.op2);
                } break;

                case FMT_REG_IMM: {
                    ptr += bytecode_emit_reg(buf, ptr, stmt->stmt_instr.op1);
                    ptr += bytecode_emit_imm(buf, ptr, stmt->stmt_instr.op2);
                } break;

                case FMT_REG_IMM8: {
                } break;

                case FMT_REG_REG: {
                    ptr += bytecode_emit_reg(buf, ptr, stmt->stmt_instr.op1);
                    ptr += bytecode_emit_reg(buf, ptr, stmt->stmt_instr.op2);
                } break;

                case FMT_REG_MEM: {
                    ptr += bytecode_emit_reg(buf, ptr, stmt->stmt_instr.op1);
                    ptr += bytecode_emit_addr(buf, ptr, stmt->stmt_instr.op2);
                } break;

                case FMT_MEM_REG: {
                    ptr += bytecode_emit_addr(buf, ptr, stmt->stmt_instr.op1);
                    ptr += bytecode_emit_reg(buf, ptr, stmt->stmt_instr.op2);
                } break;

                case FMT_REG: {
                    ptr += bytecode_emit_reg(buf, ptr, stmt->stmt_instr.op1);
                } break;

                case FMT_IMM: {
                    ptr += bytecode_emit_imm(buf, ptr, stmt->stmt_instr.op1);
                } break;

                case FMT_MEM: {
                    ptr += bytecode_emit_addr(buf, ptr, stmt->stmt_instr.op1);
                } break;

                default: {
                    assert(!"unbekanntes format");
                } break;
            }
        } else if ( stmt->kind == STMT_LABEL ) {
            size_t addr = ptr;
            sym_add(stmt->stmt_label.val, ref(addr));
            sym_resolve16(buf, stmt->stmt_label.val, (uint16_t)addr);
        } else if ( stmt->kind == STMT_DATA8 ) {
            size_t addr = ptr;
            sym_add(stmt->stmt_data8.name, ref(addr));

            for ( int elem_idx = 0; elem_idx < stmt->stmt_data8.elems.num_elems; ++elem_idx ) {
                Expr *elem = (Expr *)queue_entry(&stmt->stmt_data8.elems, elem_idx);
                ptr += bytecode_emit(buf, addr + ptr, (uint8_t)(elem->expr_imm.val & 0xff));
            }

            sym_resolve8(buf, stmt->stmt_data16.name, (uint8_t)addr);
        } else if ( stmt->kind == STMT_DATA16 ) {
            size_t addr = ptr;
            sym_add(stmt->stmt_data16.name, ref(addr));

            for ( int elem_idx = 0; elem_idx < stmt->stmt_data16.elems.num_elems; ++elem_idx ) {
                Expr *elem = (Expr *)queue_entry(&stmt->stmt_data16.elems, elem_idx);
                ptr += bytecode_emit16(buf, addr + ptr, elem->expr_imm.val);
            }

            sym_resolve16(buf, stmt->stmt_data16.name, (uint16_t)addr);
        } else if ( stmt->kind == STMT_CONST ) {
            size_t addr = ptr;
            uint16_t val = bytecode_expr16(ptr, stmt->stmt_const.val);
            sym_add(stmt->stmt_const.name, ref(val));
            sym_resolve16(buf, stmt->stmt_const.name, (uint16_t)addr);
        } else if ( stmt->kind == STMT_STRUCT ) {
            auto s = bytecode_struct(stmt->stmt_struct.name);
            map_put(&structs, stmt->stmt_struct.name, s);

            size_t field_offset = 0;
            for ( int field_index = 0; field_index < stmt->stmt_struct.elems.num_elems; ++field_index ) {
                Struct_Field *field = (Struct_Field *)queue_entry(&stmt->stmt_struct.elems, field_index);
                map_put(&s->fields, field->name, bytecode_field(field->name, field->size, field_offset));
                field_offset += field->size;
            }
        } else {
            assert(!"unbekannte anweisung");
        }
    }

    return program(buf, sizeof(buf));
}

