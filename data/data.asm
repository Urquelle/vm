struct Player {
    width  : $2,
    height : $2,
    x      : $2,
    y      : $2,
}

const CODE_CONSTANT = $DEC0
+data16 hero        = { $12, $34, $56, $78 }

' kommentar bis später
code:
    mov [!CODE_CONSTANT], &1234
    mov &[ <Player> hero.x ], r1
    hlt
