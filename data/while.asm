    mov $0A, &0050
loop:
    mov &0050, acc
    jeq $00, &[!end]
    dec acc
    mov acc, &0050
    inc r2
    inc r2
    inc r2
    jmp &[!loop]
end:
    hlt
