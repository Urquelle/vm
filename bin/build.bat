@echo off

set compiler_flags=-Od -MTd -nologo -fp:fast -fp:except- -Gm- -GR- -EHa- -Zo -Oi -WX -W4 -wd4624 -wd4530 -wd4201 -wd4100 -wd4101 -wd4189 -wd4505 -wd4127 -wd4702 -wd4310 -FC -Z7 -I%PROJECT_PATH%/src
set linker_flags= -incremental:no -opt:ref user32.lib gdi32.lib winmm.lib Shlwapi.lib Ws2_32.lib

IF NOT exist %BUILD_PATH% ( mkdir %BUILD_PATH% )
pushd %BUILD_PATH%

cl %compiler_flags% %PROJECT_PATH%\src\main.cpp -Fe%PROJECT_NAME%.exe /link %linker_flags%
rem cl %compiler_flags% %PROJECT_PATH%\examples\subroutine.cpp -Fevm_sub.exe /link %linker_flags%
rem cl %compiler_flags% %PROJECT_PATH%\examples\screen.cpp -Fevm_screen.exe /link %linker_flags%
rem cl %compiler_flags% %PROJECT_PATH%\examples\file.cpp -Fevm_file.exe /link %linker_flags%

popd
